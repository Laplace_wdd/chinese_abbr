#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Mon Jul 28 15:31:37 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random
from itertools import *


def proc(input_f):
	p, pp = re.compile(u"(.*)##(.*)\n"), re.compile(u"(\d)::(.*)") 
	for l in input_f:
		m = p.match(l)
		seg = m.group(1)
		case_lst = m.group(2).split(u",")
		ori, pos_lst = u"".join(seg.split()), []
		for case in case_lst:
			mc = pp.match(case)
			lb, abbr = mc.group(1), mc.group(2)
			ori_abbr= u"".join(abbr.split())
			if lb == u"2":
				pos_lst += [ori_abbr]
		print ori + u":" + u",".join(pos_lst)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
