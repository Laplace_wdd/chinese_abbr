#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : filter.py
# Creation Date : 22-07-2014
# Last Modified : Sun Aug 10 22:17:09 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, math
from itertools import *

tag_set = set([u"GEO", u"STR", u"DIR", u"SCH", u"HOT", u"RES"]) 
gram, thr = 4, 5 
digitS = u"\d|一|二|三|四|五|六|七|八|九|十"
numS = u"第?(?:(?:{0})+(?:百|千)*(?:万|亿)*)+号?".format(digitS)
numPtrn = re.compile(u"^(?:(?:{0})+)$".format(numS))
dirS = u"^(?:(?:东|南|西|北|东北|东南|西北|西南)方?)$".format(digitS)
dirPtrn = re.compile(dirS)
dp, type_dict, trans_dict = {}, {}, {}

vp = re.compile(u"[ -~]+")

def valid(cand):
	res = []
	for c in cand:
		if vp.match(c) and len(res) and vp.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def parse_loc(loc_lst):
	global dp
	loc = u"".join(loc_lst)
	if loc in dp:
		return dp[loc]
	elif loc == u"#START#": return ["START", 0, loc]
	elif loc == u"#END#": return ["END", 0, loc]
	elif loc in type_dict:
		tag = type_dict[loc]
		if tag == "GEO": return ["GEO", 0, loc]
		else:
			return [tag, 1, loc]
	elif numPtrn.match(loc):
		return ["NUM", 1, loc]
	elif len(loc_lst) == 1:
		return ["MISC", 3, loc]
	else:
		tr, score, res = "", 99999, [] 
		for k in range(1, len(loc_lst)):
			left = parse_loc(loc_lst[0:k])
			right = parse_loc(loc_lst[k:len(loc_lst)])
			if not len(left):
				print >> sys.stderr, u"".join(loc_lst[0:k])
			if not len(right):
				print >> sys.stderr, u"".join(loc_lst[k:len(loc_lst)])
			t_score = left[1]+right[1]
			if (left[0], right[0]) in trans_dict:
				t_score *= trans_dict[left[0], right[0]] 
			if t_score < score:
				score = t_score
				res = [right[0], score, [left, right]]
		dp[loc] = res
		return res

def tree2lst(tr):
	if type(tr[2]) is list:
		return tree2lst(tr[2][0]) + tree2lst(tr[2][1])
	return [u"{0}/{1}".format(tr[2],tr[0])]

def proc_pseudotag(input_f):
	global type_dict
	old_lb, group_lb, lst = u"", u"", []
	pdict = re.compile(u"(\S+)\t(\S+)\t(\S+)")
	for l in input_f:
		m = pdict.match(l)
		lb, wd, f = m.group(1), m.group(2), int(m.group(3))
		if old_lb == u"":
			old_lb = lb
			group_lb = lb
		if wd in tag_set:
			group_lb = wd
		if lb != old_lb:
			for word in lst:
				type_dict[word] = u"RARE_{0}".format(group_lb) 
			old_lb, group_lb, lst = lb, lb, []
		lst += [wd]
	for word in lst:
		type_dict[word] = u"RARE_{0}".format(group_lb) 

def decode(input_f):
	global type_dict, trans_dict
	pdict = re.compile(u"(\S+)::(\S+)")
	for l in codecs.open(sys.argv[1], 'r', encoding='utf-8'): 
		m = pdict.match(l)
		type_dict[m.group(1)] = m.group(2)
	proc_pseudotag(codecs.open(sys.argv[2], 'r', encoding='utf-8'))
	pdict = re.compile(u"(\S+)\t(\S+)\t(\S+)")
	for l in codecs.open(sys.argv[3], 'r', encoding='utf-8'): 
		m = pdict.match(l)
		w0, w1, score = m.group(2), m.group(3), math.exp(-float(m.group(1)))
		trans_dict[w0, w1] = score 
	pla = re.compile(u"(.*)##")
	for l in input_f:
		m = pla.match(l)
		l = m.group(1)
		ll = valid(l.split())
		ll = [u"#START#"] + ll + [u"#END#"]
		tag_lst = tree2lst(parse_loc(ll))
		tag_lst = tag_lst[1:len(tag_lst)-1]
		print u" ".join(tag_lst)
		#print u"{0}:{1}".format(l.strip(), u" ".join(tag_lst))

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	decode(input_stream)

if __name__ == "__main__": main()
