#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : filter.py
# Creation Date : 22-07-2014
# Last Modified : Wed Jul 30 21:30:52 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *

def proc(input_stream):
	p = re.compile(u":|,")
	for l in input_stream:
		lst = p.split(l.strip())
		print "\n".join(lst)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	proc(input_stream)

if __name__ == "__main__": main()
