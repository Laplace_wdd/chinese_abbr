#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Thu Aug  7 09:55:31 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random
from itertools import *
from operator import itemgetter

gram, thr = 4, 5 
def proc(input_f):
	global loc_dict, para, loc_dict, para, provSet, citySet, countySet, townSet, distSet
	spec_loc_lst = []
	#provS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[1], 'r', encoding='utf-8')]
	#cityS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[2], 'r', encoding='utf-8')]
	#cntyS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[3], 'r', encoding='utf-8')]
	#townS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[4], 'r', encoding='utf-8')]
	#distS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[5], 'r', encoding='utf-8')]
	spec_loc_lst.sort(key=len, reverse=True)
	prefix = u"|".join(pref_lst)
	digitS = u"\d|一|二|三|四|五|六|七|八|九|十"
	numS = u"第?(?:(?:{0})+(?:百|千)*(?:万|亿)*)+号?".format(digitS)
	pref_lst = spec_loc_lst
	cand_lst = []
	for l in input_f:
		l = l.strip()
		cand_lst += [[l]]
	prfxStr = u"(?:{0})*(\S*?)$".format(prefix)
	prfxPtrn = re.compile(prfxStr)
	for i, item in enumerate(cand_lst):
		cand_lst[i] += [prfxPtrn.match(item[len(item)-1]).group(1)]
	#suff_street
	pref_suff_lst += [u"街", u"弄", u"路", u"巷", u"胡同", u"大道", u"社区"]
	#suff_agency
	pref_suff_lst += [u"所", u"院", u"处", u"办", u"局", u"厅", u"厂", u"社", u"委"]
	 + [u"\S*{0}".format(suff) for suff in pref_suff_lst]
	old_suffxStr = ""
	for root, brand in cand_lst:
		print u"{0}:{1}".format(root, brand)
	while 1:
		suff_lst = [u"{0}".format(suff) for suff in suff_suff_lst]
		suffix = u"|".join(suff_lst)
		suffxStr = u"(?:{0})*(\S*?)(?:{0})*(?:{1})+".format(numS, suffix)
		if suffxStr == old_suffxStr: break
		suffPtrn = re.compile(suffxStr)
		freq_dict = [{}, {}, {}, {}]
		for i, item in enumerate(cand_lst):
			m = suffPtrn.match(item[1])
			if m: cand_lst[i][1] = m.group(1)
			rem = cand_lst[i][1]
			for i in range(0, gram):
				if len(rem) > i:
					suff = rem[len(rem)-i-1:len(rem)] 
					if suff in freq_dict[i]:
						freq_dict[i][suff] += 1
					else:
						freq_dict[i][suff] = 1
		for i in range(0, gram):
			suff_suff_lst += [suff for suff in freq_dict[i] if freq_dict[i][suff] > (gram-i+1)*thr]
		old_suffxStr = suffxStr 
#	for root, brand in cand_lst:
#		print u"{0}:{1}".format(root, brand)
	print old_suffxStr
					
def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
