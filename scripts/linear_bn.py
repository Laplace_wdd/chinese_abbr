#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Thu Aug  7 14:56:14 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random
from itertools import *
from operator import itemgetter

gram, thr = 4, 5 
def proc(input_f):
	digitS = u"\d|一|二|三|四|五|六|七|八|九|十"
	numS = u"第?(?:(?:{0})+(?:百|千)*(?:万|亿)*)+号?".format(digitS)
	spec_loc_lst = []
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[1], 'r', encoding='utf-8')] #provS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[2], 'r', encoding='utf-8')] #cityS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[3], 'r', encoding='utf-8')] #cntyS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[4], 'r', encoding='utf-8')] #townS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[5], 'r', encoding='utf-8')] #distS
	spec_loc_lst.sort(key=len, reverse=True)
	street_lst = [u"街", u"弄", u"路", u"巷", u"胡同", u"里"]
	agency_lst = [u"院", u"所", u"处", u"办", u"局", u"厅", u"厂", u"社", u"委"] #suff_agency
	sch_lst = [u"小学", u"学校", u"小", u"校", u"小学校", u"高级中学",u"中学", u"高中", u"中", u"医学院", u"学院", u"大学", u"师大"] #suff_univ
	res_lst = [u"新家园", u"家属楼", u"家属院", u"小区", u"花园", u"园", u"宿舍", u"大厦", u"公寓", u"家园", u"社区", u"新村", u"社区", u"广场", u"别墅", u"山庄", u"大楼", u"大院", u"中心", u"苑"] #suff_res
	hot_lst = [ u"大酒店", u"酒店", u"宾馆", u"公寓", u"饭店", u"连锁", u"旅馆"] #suff_hot
	misc_lst = []
	type_lst = [(spec_loc_lst, "GEN"), (street_lst, "STR"), (agency_lst, "AGE"), (sch_lst, "SCH"), (res_lst, "RES"), (hot_lst, "HOT"), (misc_lst, "MISC")]
	suff_lst = spec_loc_lst + street_lst + agency_lst + sch_lst + res_lst + hot_lst + misc_lst
	suff_lst.sort(key=len, reverse=True)
	kwd_str = u"|".join(suff_lst)
	kwdP = re.compile(kwd_str)
	for l in input_f:
		l = l.strip()
		itr = kwdP.finditer(l)
		s, seg_lst = 0, []
		for m in itr:
			ns = m.span()[1]
			seg_lst += [l[s:ns]]
			s = ns
		if s != len(l):
			seg_lst += [l[s:len(l)]]
		print " ".join(seg_lst)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
