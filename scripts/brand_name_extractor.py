#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Wed Aug  6 14:52:55 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random
from itertools import *

def proc(input_f):
	global loc_dict, para, loc_dict, para, provSet, citySet, countySet, townSet, distSet
	pref_suff_lst, suff_suff_lst = [], []
	#suff_prov
	pref_suff_lst += [] #[u"自治区", u"行政区", u"省"]
	#suff_city
	pref_suff_lst += [] #[u"市", u"自治州", u"自治县", u"盟", u"行政区"]
	#suff_cnty
	pref_suff_lst += [] #[u"镇"]
	#suff_town
	pref_suff_lst += [] #[u"自治县", u"旗", u"县", u"市"]
	#suff_dist
	pref_suff_lst += [] #[u"区"]
	#suff_street
	pref_suff_lst += [u"街", u"弄", u"路", u"巷", u"胡同", u"大道", u"社区"]
	#suff_agency
	pref_suff_lst += [u"所", u"院", u"处", u"办", u"局", u"厅", u"厂", u"社", u"委"]
	#suff_school
	suff_suff_lst += [u"小学", u"学校", u"小", u"校", u"小学校", u"高级中学",u"中学", u"学校", u"高中", u"中"]
	#suff_univ
	suff_suff_lst += [u"医学院", u"学院", u"院", u"大学", u"学校", u"师大"]
	#suff_res
	suff_suff_lst += [u"新家园", u"家属楼", u"家属院", u"小区", u"花园", u"园", u"宿舍", u"大厦", u"公寓", u"家园", u"社区", u"新村", u"社区", u"广场", u"别墅", u"山庄", u"大楼", u"大院", u"中心", u"院", u"巷", u"都", u"苑", u"庭院"]
	#suff_hot
	suff_suff_lst += [ u"大酒店", u"酒店", u"宾馆", u"公寓", u"饭店", u"连锁", u"旅馆"]
	#provS = u"{0}|{1}".format(u"|".join([l.strip() for l in codecs.open(sys.argv[1], 'r', encoding='utf-8')]), u"|".join([u"\S*{0}".format(suff) for suff in suff_prov]))
	#cityS = u"{0}|{1}".format(u"|".join([l.strip() for l in codecs.open(sys.argv[2], 'r', encoding='utf-8')]), u"|".join([u"\S*{0}".format(suff) for suff in suff_city]))
	#cntyS = u"{0}|{1}".format(u"|".join([l.strip() for l in codecs.open(sys.argv[3], 'r', encoding='utf-8')]), u"|".join([u"\S*{0}".format(suff) for suff in suff_cnty]))
	#townS = u"{0}|{1}".format(u"|".join([l.strip() for l in codecs.open(sys.argv[4], 'r', encoding='utf-8')]), u"|".join([u"\S*{0}".format(suff) for suff in suff_town]))
	#distS = u"{0}|{1}".format(u"|".join([l.strip() for l in codecs.open(sys.argv[5], 'r', encoding='utf-8')]), u"|".join([u"\S*{0}".format(suff) for suff in suff_dist]))
	spec_loc_lst = []
	#provS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[1], 'r', encoding='utf-8')]
	#cityS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[2], 'r', encoding='utf-8')]
	#cntyS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[3], 'r', encoding='utf-8')]
	#townS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[4], 'r', encoding='utf-8')]
	#distS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[5], 'r', encoding='utf-8')]
	spec_loc_lst.sort(key=len, reverse=True)

	pref_lst = spec_loc_lst + [u"\S*{0}".format(suff) for suff in pref_suff_lst]
	suff_lst = [u"{0}".format(suff) for suff in suff_suff_lst]
	digitS = u"\d|一|二|三|四|五|六|七|八|九|十"
	numS = u"第?(?:(?:{0})+(?:百|千)*(?:万|亿)*)+号?".format(digitS)
	prefix = u"|".join(pref_lst)
	suffix = u"|".join(suff_lst)
	p1Str = u"(?:{0})*(?:{1})*(\S*?)(?:{1})*(?:{2})+".format(prefix, numS, suffix)
	pt1 = re.compile(p1Str)
	for l in input_f:
		l = l.strip()
		m = pt1.match(l)
		if m:
			print u"{0}:{1}".format(l, m.group(1))
			continue
		print u"{0}:{1}".format(l, "")

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
