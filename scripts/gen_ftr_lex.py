#! /usr/bin/python
# --------------------------------------- 
# File Name : gen_ftr_lex.py
# Creation Date : 18-07-2014
# Last Modified : Wed Jul 23 19:09:03 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle
import codecs
from itertools import *

word2vec, bos_v, eos_v, oov_v, num_v, pu_v, eng_v = {}, [], [], [], [], [], []

def load_dict():
	global word2vec, bos_v, eos_v, oov_v, num_v, pu_v, eng_v 
	dict_f = codecs.open(sys.argv[1], 'r', encoding='utf-8')
	for l in dict_f:
		fea_lst = l.split()
		if fea_lst[0] == u"<BOS>":
			bos_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<EOS>":
			eos_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<OOV>":
			oov_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<NUM>":
			num_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<PU>":
			pu_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<ENG>":
			eng_v = fea_lst[1:len(fea_lst)]
		else:
			word2vec[fea_lst[0]] = fea_lst[1:len(fea_lst)]
	dict_f.close()

def proc(input_f):
	global word2vec, bos_v, eos_v, oov_v, num_v, pu_v, eng_v 
	prev_vec, idx, prev_sent = bos_v, 1, 0
	p = re.compile(u"(\d qid:(\d+) )fid:\d+ (\S+)") 
	for l in input_f:
		m = p.match(l)
		cur_word = m.group(3)
		cur_sent = m.group(2)
		cur_tt = m.group(1)
		cur_vec = []
		if cur_word in word2vec:
			cur_vec = word2vec[cur_word]
		elif re.compile(u"[A-Za-z]+").match(cur_word):
			cur_vec = eng_v
		elif re.compile(u"[0-9]+").match(cur_word):
			cur_vec = num_v
		elif re.compile(u"\W").match(cur_word):
			cur_vec = pu_v
		else:
			cur_vec = oov_v
		vec_append = cur_vec
		if cur_sent != prev_sent:
			vec_append = eos_v
			prev_vec = bos_v
		if prev_sent:
			print " ".join([str(i+idx) + ":" + vec_append[i] for i in range(0, len(vec_append))]) 
			#print
		idx = 1
		print cur_tt + " ".join([str(i+idx) + ":" + prev_vec[i] for i in range(0, len(prev_vec))]), 
		idx += len(prev_vec)
		print " ".join([str(i+idx) + ":" + cur_vec[i] for i in range(0, len(cur_vec))]), 
		idx += len(cur_vec)
		prev_vec = cur_vec
		prev_sent = cur_sent
	print " ".join([str(i+idx) + ":" + eos_v[i] for i in range(0, len(eos_v))]) 

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	load_dict()
	proc(input_stream)
	
if __name__ == "__main__": main()
