#! /usr/bin/python
# --------------------------------------- 
# File Name : statistics.py
# Creation Date : 21-07-2014
# Last Modified : Fri Jul 25 13:54:37 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, numpy, codecs 
from itertools import *

vp = re.compile(u"[ -~]+")

def valid(cand):
	global vp
	res = []
	for c in cand:
		if vp.match(c) and len(res) and vp.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def stat(input_f):
	p = re.compile(u"(\S+):(\S+)\n")
	qlen, abbr_len = [], [] 
	for l in input_f:
		m = p.match(l)
		if m:
			cand = valid(m.group(1))
			abbr_lst = m.group(2).split(u",") 
			qlen += [len(cand)]
			abbr_len += [len(abbr) for abbr in abbr_lst]

	print "Average length (Query):" + str(numpy.mean(qlen))
	print "Maximum length (Query):" + str(numpy.max(qlen))
	print "Average length (Abbr):" + str(numpy.mean(abbr_len))
	print "Average length (Abbr):" + str(numpy.max(abbr_len))
#	print len(qlen)
#	print sorted(qlen, reverse=True)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	stat(input_stream)

if __name__ == "__main__": main()
