#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Fri Aug 15 19:01:05 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random
from itertools import *

p, ptag = re.compile(u"[ -~]+"), re.compile(u"(\S+)/(\S+)")
loc_dict, para, provSet, citySet, countySet, townSet, distSet = set(), set(), set(), set(), set(), set(), set()
w2freq, w2pos, w2brown, struct_lst = {}, {}, {}, []

def valid(cand):
	res = []
	for c in cand:
		if p.match(c) and len(res) and p.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def decode(seg, abbr):
	arr, abbr_v, res = valid(seg.split()), abbr.split(), []
	for j, word in enumerate(arr):
		if p.match(word) or len(word) == 1:
			res += [[0, word, word, 0, j]]
		else:
			for i in range(0, len(word)):
				res += [[0, word[i], word, i, j]]
	i = 0
	for itm in res:
		if abbr_v[i] == itm[1]:
			i += 1
			itm[0] = 1
		if i == len(abbr_v): break
	if i != len(abbr_v):
		print >> sys.stderr, str(i) + ":" + str(len(abbr_v))
		print >> sys.stderr, "Wrong!", seg + ":" + u" ".join(abbr_v) 
	return res

def isNum(cha):
	pdigit = re.compile(u"\d+|一|二|三|四|五|六|七|八|九|十|百|千|万|亿")
	if pdigit.match(cha):
		return True
	return False

def isProv(word):
	if word in provSet:
		return True
	return False

def isCity(word):
	if word in citySet:
		return True
	return False

def isCnty(word):
	if word in countySet:
		return True
	return False

def isTown(word):
	if word in townSet:
		return True

def isDist(word):
	if word in distSet:
		return True
	return False

def sufProv(word, iidx):
	if isProv(word):
		lst = [u"自治区", u"行政区", u"省"]
		for suf in lst:
			if word.endswith(suf) and iidx < len(suf):
				return suf 
	return u"" 

def sufCity(word, iidx):
	if isCity(word):
		lst = [u"市", u"自治州", u"自治县", u"盟", u"行政区"]
		for suf in lst:
			if word.endswith(suf) and iidx < len(suf):
				return suf 
	return u""

def sufCnty(word, iidx):
	if isCnty(word):
		lst = [u"自治县", u"旗", u"县", u"市"]
		for suf in lst:
			if word.endswith(suf) and iidx < len(suf):
				return suf 
	return u""

def sufTown(word, iidx):
	if isTown(word):
		lst = [u"镇"]
		for suf in lst:
			if word.endswith(suf) and iidx < len(suf):
				return suf 
	return u""

def sufDist(word, iidx):
	if isDist(word):
		lst = [u"区"]
		for suf in lst:
			if word.endswith(suf) and iidx < len(suf):
				return suf 
	return u""

def sufPSchool(word, iidx):
	lst = [u"小学", u"学校", u"小", u"小学校"]
	for suf in lst:
		if word.endswith(suf) and iidx < len(suf):
			return suf 
	return u""

def sufRes(word, iidx):
	lst = [u"家属楼", u"小区", u"花园", u"园", u"宿舍", u"大厦", u"公寓", u"家园", u"社区", u"新村", u"社区", u"广场", u"别墅", u"山庄", u"大楼", u"大楼", u"大院", u"中心", u"院", u"巷", u"都", u"苑"]
	for suf in lst:
		if word.endswith(suf) and iidx < len(suf):
			return suf 
	return u""

def sufSSchool(word, iidx):
	lst = [u"中学", u"学校", u"高中", u"中"]
	for suf in lst:
		if word.endswith(suf) and iidx < len(suf):
			return suf 
	return u""

def sufUniv(word, iidx):
	lst = [u"医学院", u"学院", u"院", u"大学", u"学校", u"师大"]
	for suf in lst:
		if word.endswith(suf) and iidx < len(suf):
			return suf 
	return u""

def sufHot(word, iidx):
	lst = [ u"大酒店", u"酒店", u"宾馆", u"公寓", u"饭店", u"连锁", u"旅馆"]
	for suf in lst:
		if word.endswith(suf) and iidx < len(suf):
			return suf 
	return u""

def sufSchool(word, iidx):
	lst = [ u"学校", u"党校", u"分校", u"校"]
	for suf in lst:
		if word.endswith(suf) and iidx < len(suf):
			return suf 
	return u""

def template_c0(info_lst):
	#c01:character[i]==c&&keep[i] == TRUE
	#c00:character[i]==c&&keep[i] == FLASE 
	ftr = []
	for info in info_lst:
		ftr += [u"<c0{0}:{1}>".format(str(info[0]), info[1])] 
	return ftr

def template_cm1(info_lst):
	#cm1T:character[i-1]==c&&keep[i] == TRUE
	#cm1F:character[i-1]==c&&keep[i] == FLASE 
	ftr = []
	for i, info in enumerate(info_lst):
		if i == 0:
			ftr += [u"<cm1{0}:<BOS>>".format(str(info[0]))]
		else:
			ftr += [u"<cm1{0}:{1}>".format(str(info[0]), info_lst[i-1][1])]
	return ftr

def template_cp1(info_lst):
	#cp11:character[i+1]==c&&keep[i] == TRUE
	#cp10:character[i+1]==c&&keep[i] == FLASE 
	ftr = []
	for i, info in enumerate(info_lst):
		if i == len(info_lst) - 1:
			ftr += [u"<cp1{0}:<EOS>>".format(str(info[0]))]
		else:
			ftr += [u"<cp1{0}:{1}>".format(str(info[0]), info_lst[i+1][1])]
	return ftr

def template_isNum(info_lst):
	#Num1:character[i] is number&&keep[i] == TRUE
	#Num0:character[i] is number&&keep[i] == FLASE 
	ftr = []
	for info in info_lst:
		if isNum(info[1]):
			ftr += [u"<Num{0}>".format(str(info[0]))]
	return ftr

def template_cw(info_lst):
	#cw1:character[i] == c && word[j] = w && cwMap[i,j] && keep[i] == TRUE
	#cw0:character[i] == c && word[j] = w && cwMap[i,j] && keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		ftr += [u"<cw{0}:{1}:{2}>".format(str(info[0]), info[1], info[2])]
	return ftr

def template_glb(info_lst):
	#DEL_GT1: number of deleted characters > 1
	#REM_GT1: number of remained characters > 1 
	ftr = []
	nrem = len([info for info in info_lst if info[0]])
	ndel = len(info_lst) - nrem
	if ndel > 1:
		ftr += [u"<DEL_GT1>"]
	if nrem > 1:
		ftr += [u"<REM_GT1>"]
	return ftr

def template_cL(info_lst):
	#cL1:character[i] == c && Location(word[j]) && cwMap[i,j] && keep[i] == TRUE
	#cL0:character[i] == c && Location(word[j]) && cwMap[i,j] && keep[i] == FALSE 
	ftr = []
	global loc_dict
	for info in info_lst:
		if info[2] in loc_dict:
			ftr += [u"<cL{0}:{1}>".format(str(info[0]), info[1])]
	return ftr

def template_cm1c0W(info_lst):
	#cm1c0W11:character[i-1] == c1 && character[i]  == c2 && cwMap[i-1,j] && cwMap[i, j] && keep[i-1] &&keep[i]
	#cm1c0W01:character[i-1] == c1 && character[i]  == c2 && cwMap[i-1,j] && cwMap[i, j] && !keep[i-1] && keep[i]
	#cm1c0W10:character[i-1] == c1 && character[i]  == c2 && cwMap[i-1,j] && cwMap[i, j] && keep[i-1] &&keep[i]
	#cm1c0W00:character[i-1] == c1 && character[i]  == c2 && cwMap[i-1,j] && cwMap[i, j] && keep![i-1] &&keep[i]
	ftr = []
	for i in range(1, len(info_lst)):
		if info_lst[i][4] == info_lst[i-1][4]:
			ftr+= [u"<cm1c0W{0}{1}:{2}:{3}>".format(str(info_lst[i-1][0]), str(info_lst[i][0]), info_lst[i-1][1], info_lst[i][1])]
	return ftr

def template_cp1c0W(info_lst):
	#cp1c0W11:character[i-1] == c1 && character[i]  == c2 && cwMap[i-1,j] && cwMap[i, j] && keep[i-1] &&keep[i]
	#cp1c0W01:character[i-1] == c1 && character[i]  == c2 && cwMap[i-1,j] && cwMap[i, j] && !keep[i-1] && keep[i]
	#cp1c0W10:character[i-1] == c1 && character[i]  == c2 && cwMap[i-1,j] && cwMap[i, j] && keep[i-1] &&keep[i]
	#cp1c0W00:character[i-1] == c1 && character[i]  == c2 && cwMap[i-1,j] && cwMap[i, j] && keep[i-1] &&keep[i]
	ftr = []
	for i in range(0, len(info_lst)-1):
		if info_lst[i][4] == info_lst[i+1][4]:
			ftr+= [u"<cp1c0W{0}{1}:{2}:{3}>".format(str(info_lst[i+1][0]), str(info_lst[i][0]), info_lst[i+1][1], info_lst[i][1])]
	return ftr

def template_cwLASTW(info_lst):
	#cwLASTW1:character[i] == c && word[j] = w && cwMap[i,j] && last(j) && keep[i] == TRUE
	#cwLASTW0:character[i] == c && word[j] = w && cwMap[i,j] && last(j) && keep[i] == FALSE 
	ftr = []
	last_idx = info_lst[len(info_lst)-1][4]
	for info in info_lst:
		if info[4] == last_idx:
			ftr += [u"<cwLASTW{0}:{1}:{2}>".format(str(info[0]), info[1], info[2])]
	return ftr

def template_cw0wp1wp2L(info_lst):
	#cw0wp1wp2L1:c_i && w_j && cwMap[i,j] && Location(j) && w_j+1 && w_j+2 && keep[i] == TRUE
	#cw0wp1wp2L0:c_i && w_j && cwMap[i,j] && Location(j) && w_j+1 && w_j+2 keep[i] == FALSE 
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	w_lst = [u""]*seq_len
	for info in info_lst:
		w_lst[info[4]] = info[2]
	global loc_dict
	for info in info_lst:
		if info[2] in loc_dict:
			w_idx = info[4]
			if w_idx + 1 < len(w_lst):
				wp1 = w_lst[w_idx + 1]
				wp2 = u"<EOS>"
				if w_idx + 2 < len(w_lst):
					wp2 = w_lst[w_idx + 2]
				ftr += [u"<cw0wp1wp2L{0}:{1}:{2}:{3}:{4}>".format(str(info[0]), info[1], info[2], wp1, wp2)]
	return ftr

def template_wm1(info_lst):
	#wm11: C_i && cwMap[i,j] && w_j-1 && keep[i] == TRUE
	#wm10: C_i && cwMap[i,j] && w_j-1 && keep[i] == FALSE 
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	w_lst = [u""]*seq_len
	for info in info_lst:
		w_lst[info[4]] = info[2]
	for info in info_lst:
		w_idx = info[4]
		wm1 = u"<BOS>"
		if w_idx > 0:
			wm1 = w_lst[w_idx-1]
		ftr += [u"<wm1{0}:{1}>".format(str(info[0]), wm1)]
	return ftr

def template_wp1(info_lst):
	#wp11: C_i && cwMap[i,j] && w_j+1 && keep[i] == TRUE
	#wp10: C_i && cwMap[i,j] && w_j+1 && keep[i] == FALSE 
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	w_lst = [u""]*seq_len
	for info in info_lst:
		w_lst[info[4]] = info[2]
	for info in info_lst:
		w_idx = info[4]
		wp1 = u"<EOS>"
		if w_idx + 1< len(w_lst):
			wp1 = w_lst[w_idx+1]
		ftr += [u"<wp1{0}:{1}>".format(str(info[0]), wp1)]
	return ftr

def template_wp2(info_lst):
	#wp21: C_i && cwMap[i,j] && w_j+2 && keep[i] == TRUE
	#wp20: C_i && cwMap[i,j] && w_j+2 && keep[i] == FALSE 
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	w_lst = [u""]*seq_len
	for info in info_lst:
		w_lst[info[4]] = info[2]
	for info in info_lst:
		w_idx = info[4]
		wp2 = u"<EOS>"
		if w_idx + 2 < len(w_lst):
			wp2 = w_lst[w_idx+2]
		ftr += [u"<wp2{0}:{1}>".format(str(info[0]), wp2)]
	return ftr

def template_wp3(info_lst):
	#wp31: C_i && cwMap[i,j] && w_j+3 && keep[i] == TRUE
	#wp30: C_i && cwMap[i,j] && w_j+3 && keep[i] == FALSE 
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	w_lst = [u""]*seq_len
	for info in info_lst:
		w_lst[info[4]] = info[2]
	for info in info_lst:
		w_idx = info[4]
		wp3 = u"<EOS>"
		if w_idx+3 < len(w_lst):
			wp3 = w_lst[w_idx+3]
		ftr += [u"<wp3{0}:{1}>".format(str(info[0]), wp3)]
	return ftr

def template_lsp(info_lst):
	#lsp1: #w && pos(i, j)&& keep[i] == TRUE
	#lsp0: #w && pos(i, j)&& keep[i] == FALSE 
	seq_len = info_lst[len(info_lst)-1][4] + 1
	ftr = []
	for info in info_lst:
		ftr += [u"<lsp{0}:{1}:{2}>".format(str(info[0]), str(seq_len), str(info[3]))]
	return ftr

def template_lfp(info_lst):
	#lfp1: #c && pos(i, j)&& keep[i] == TRUE
	#lfp0: #c && pos(i, j)&& keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		ftr += [u"<lfp{0}:{1}:{2}>".format(str(info[0]), str(len(info_lst)), str(info[3]))]
	return ftr

def template_lwp(info_lst):
	#lwp1: C_i && cwMap[i,j] && len(j) && pos(i, j)&& keep[i] == TRUE
	#lwp0: C_i && cwMap[i,j] && len(j) && pos(i, j)&& keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		ftr += [u"<lwp{0}:{1}:{2}>".format(str(info[0]), str(len(info[2])), str(info[3]))]
	return ftr

def template_CWp(info_lst):
	#CWp1: C_i && cwMap[i,j] && W_j && pos(i, j) && keep[i] == TRUE
	#CWp0: C_i && cwMap[i,j] && W_j && pos(i, j) && keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		ftr += [u"<CWp{0}:{1}>".format(str(info[0]), str(info[3]))]
	return ftr

def template_CWRp(info_lst):
	#CWRp1: C_i && cwMap[i,j] && W_j && Rpos(i, j) && keep[i] == TRUE
	#CWRp0: C_i && cwMap[i,j] && W_j && Rpos(i, j) && keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		ftr += [u"<CWRp{0}:{1}>".format(str(info[0]), str(len(info[2])-1-info[3]))]
	return ftr

def template_CWpwp(info_lst):
	#CWpwp1: C_i && cwMap[i,j] && W_j && pos(W_j) && pos(i,j) && keep[i] == TRUE
	#CWpwp0: C_i && cwMap[i,j] && W_j && pos(W-j) && pos(i,j) &&keep[i] == TRUE
	ftr = []
	for info in info_lst:
		ftr += [u"<CWpwp{0}:{1}:{2}>".format(str(info[0]), str(info[3]), str(info[4]))]
	return ftr

def template_CWpRwp(info_lst):
	#CWpwp1: C_i && cwMap[i,j] && W_j && pos(W_j) && pos(i,j) && keep[i] == TRUE
	#CWpwp0: C_i && cwMap[i,j] && W_j && pos(W-j) && pos(i,j) &&keep[i] == TRUE
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	for info in info_lst:
		ftr += [u"<CWpRwp{0}:{1}:{2}>".format(str(info[0]), str(info[3]), str(seq_len-1-info[4]))]
	return ftr

def template_CWpRwp(info_lst):
	#CWpwp1: C_i && cwMap[i,j] && W_j && pos(W_j) && pos(i,j) && keep[i] == TRUE
	#CWpwp0: C_i && cwMap[i,j] && W_j && pos(W-j) && pos(i,j) &&keep[i] == TRUE
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	for info in info_lst:
		ftr += [u"<CWpRwp{0}:{1}:{2}>".format(str(info[0]), str(info[3]), str(seq_len-1-info[4]))]
	return ftr

def template_PW(info_lst):
	ftr = []
	code, sidx = u"", 0
	for info in info_lst:
		if info[4] != sidx:
			ftr += [u"<PW:{0}>".format(code)]
			code, sidx = u"", info[4]
		code += str(info[0])
	return ftr

def template_PF(info_lst):
	return [u"<PF:{0}>".format(u"".join([str(info[0]) for info in info_lst]))]

#def template_Pw(info_lst):
#	return [u"<PF:{0}>".format(u"".join([str(info[0]) for info in info_lst]))]


def template_LTm1(info_lst):
	#cTm1: C_i && cwMap[i,j] && isT(W_j-1) && keep[i] == TRUE
	#cTm0: C_i && cwMap[i,j] && isT(W_j-1) && keep[i] == FALSE 
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	w_lst = [u""]*seq_len
	for info in info_lst:
		w_lst[info[4]] = info[2]
	for info in info_lst:
		w_idx = info[4]
		if w_idx > 0:
			w_idx -= 1
			if isProv(w_lst[w_idx]):
				ftr += [u"<cProvm1{0}>".format(str(info[0]))]
			elif isCity(w_lst[w_idx]):
				ftr += [u"<cCitym1{0}>".format(str(info[0]))]
			elif isCnty(w_lst[w_idx]):
				ftr += [u"<cCntym1{0}>".format(str(info[0]))]
			elif isTown(w_lst[w_idx]):
				ftr += [u"<cTownm1{0}>".format(str(info[0]))]
			elif isDist(w_lst[w_idx]):
				ftr += [u"<cDistm1{0}>".format(str(info[0]))]
	return ftr

def template_LT0(info_lst):
	#cT01: C_i && cwMap[i,j] && isT(W_j) && keep[i] == TRUE
	#cT00: C_i && cwMap[i,j] && isT(W_j) && keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		if isProv(info[2]):
			ftr += [u"<cProv0{0}>".format(str(info[0]))]
		elif isCity(info[2]):
			ftr += [u"<cCty0{0}>".format(str(info[0]))]
		elif isCnty(info[2]):
			ftr += [u"<cCtny0{0}>".format(str(info[0]))]
		elif isTown(info[2]):
			ftr += [u"<cTown0{0}>".format(str(info[0]))]
		elif isDist(info[2]):
			ftr += [u"<cDist0{0}>".format(str(info[0]))]
	return ftr

def template_LTp1(info_lst):
	#cTp1: C_i && cwMap[i,j] && isT(W_j+1) && keep[i] == TRUE
	#cTp0: C_i && cwMap[i,j] && isT(W_j+1) && keep[i] == FALSE 
	ftr = []
	seq_len = info_lst[len(info_lst)-1][4] + 1
	w_lst = [u""]*seq_len
	for info in info_lst:
		w_lst[info[4]] = info[2]
	for info in info_lst:
		w_idx = info[4]
		if w_idx + 1< len(w_lst):
			w_idx += 1
			if isProv(w_lst[w_idx]):
				ftr += [u"<cProvp1{0}>".format(str(info[0]))]
			elif isCity(w_lst[w_idx]):
				ftr += [u"<cCtyp1{0}>".format(str(info[0]))]
			elif isCnty(w_lst[w_idx]):
				ftr += [u"<cCntyp1{0}>".format(str(info[0]))]
			elif isTown(w_lst[w_idx]):
				ftr += [u"<cTownp1{0}>".format(str(info[0]))]
			elif isDist(w_lst[w_idx]):
				ftr += [u"<cDistp1{0}>".format(str(info[0]))]
	return ftr

def template_suffixcwL(info_lst):
	#sufT01: C_i && cwMap[i,j] && isSufT(W_j) && keep[i] == TRUE
	#sufT00: C_i && cwMap[i,j] && isSufT(W_j) && keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		suf = sufProv(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwProv{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufCity(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwCty{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufCnty(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwCtny{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufTown(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwTown{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufDist(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwDist{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufPSchool(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwPSchool{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufSSchool(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwSSchool{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufRes(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwRes{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufUniv(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwUniv{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufHot(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwHot{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
		suf = sufSchool(info[2], len(info[2]) - 1 - info[3])
		if suf != u"":
			ftr += [u"<sufcwSchool{0}:{1}:{2}>".format(str(info[0]), info[1], suf)]
	return ftr

def template_WFreq(info_lst):
	global w2freq
	#cw1:character[i] == c && word[j] = w && cwMap[i,j] && keep[i] == TRUE
	#cw0:character[i] == c && word[j] = w && cwMap[i,j] && keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		freq = u"<OOV>"
		if info[2] in w2freq:
			freq = w2freq[info[2]]
		ftr += [u"<WFreq{0}:{1}:{2}>".format(str(info[0]), info[3], freq)]
	return ftr

def template_WPos(info_lst):
	global w2pos
	#cw1:character[i] == c && word[j] = w && cwMap[i,j] && keep[i] == TRUE
	#cw0:character[i] == c && word[j] = w && cwMap[i,j] && keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		pos_lst = [u"<OOV>"]
		if info[2] in w2pos:
			pos_lst = w2pos[info[2]]
		for pos in pos_lst:
			ftr += [u"<WPos{0}:{1}:{2}>".format(str(info[0]), info[3], pos)]
	return ftr

def template_WBrown(info_lst):
	global w2brown
	#cw1:character[i] == c && word[j] = w && cwMap[i,j] && keep[i] == TRUE
	#cw0:character[i] == c && word[j] = w && cwMap[i,j] && keep[i] == FALSE 
	ftr = []
	for info in info_lst:
		brown = u"<OOV>"
		if info[2] in w2brown:
			brown = w2brown[info[2]]
		ftr += [u"<WBrown{0}:{1}:{2}>".format(str(info[0]), info[3], brown)]
	return ftr

def template(seg, abbr):
	info_lst, ftr = decode(seg, abbr), [] 
	#-----glb
	if "1" in para: 
		ftr += template_c0(info_lst)
		ftr += template_cm1(info_lst)
		ftr += template_cp1(info_lst)
		ftr += template_cw(info_lst)
		ftr += template_isNum(info_lst)
	#-----glb_len_loc
	if "2" in para:
		ftr += template_glb(info_lst)
		ftr += template_cL(info_lst)
	#-----glb_cc
	if "3" in para:
		ftr += template_cm1c0W(info_lst)
		ftr += template_cp1c0W(info_lst)
	#-----glb_cc_cw
	if "3.5" in para:
		ftr += template_cwLASTW(info_lst)
		ftr += template_cw0wp1wp2L(info_lst)
	if "4" in para:
		ftr += template_wm1(info_lst)
		ftr += template_wp1(info_lst)
		ftr += template_wp2(info_lst)
		ftr += template_wp3(info_lst)
	if "5" in para:
		ftr += template_lsp(info_lst)
		ftr += template_lfp(info_lst)
		ftr += template_lwp(info_lst)
		ftr += template_CWp(info_lst)
		ftr += template_CWRp(info_lst)
		ftr += template_CWpwp(info_lst)
		ftr += template_CWpRwp(info_lst)
	if "6" in para:
		ftr += template_PW(info_lst) 
		ftr += template_PF(info_lst) 
	if "7" in para:
		ftr += template_LTm1(info_lst) 
		ftr += template_LT0(info_lst) 
		ftr += template_LTp1(info_lst) 
		ftr += template_suffixcwL(info_lst)
	if "8" in para:
		ftr += template_WFreq(info_lst)
		ftr += template_WPos(info_lst)
		ftr += template_WBrown(info_lst)
	return u" ".join(ftr)

def decode_str(seg, abbr):
	arr, abbr_v, res = seg.split(), abbr.split(), []
	for j, w2t in enumerate(arr):
		m = ptag.match(w2t)
		word, tag = valid(m.group(1)), m.group(2)
		for i in range(0, len(word)):
			res += [[0, word[i], tag, i, j]]
	i = 0
	for itm in res:
		if abbr_v[i] == itm[1]:
			i += 1
			itm[0] = 1
		if i == len(abbr_v): break
	if i != len(abbr_v):
		print >> sys.stderr, str(i) + ":" + str(len(abbr_v))
		print >> sys.stderr, "Wrong!", seg + ":" + u" ".join(abbr_v) 
	return res

def template_Wtag(info_lst):
	ftr = []
	for info in info_lst:
		ftr += [u"<Wtag{0}:{1}>".format(str(info[0]), info[2])]
	return ftr

def template_Brand(info_lst):
	ftr = []
	for info in info_lst:
		if info[2] == u"0":
			ftr += [u"<Brand{0}>".format(str(info[0]))]
	return ftr

def template_struct(seg, abbr):
	info_lst, ftr = decode_str(seg, abbr), []
	if "9.5" in para:
		ftr += template_Brand(info_lst)
	else:
		ftr += template_Wtag(info_lst)
	return u" ".join(ftr)

def proc(input_f):
	p, pp = re.compile(u"(.*)##(.*)\n"), re.compile(u"(\d)::(.*)") 
	qid = 1
	global loc_dict, para, loc_dict, para, provSet, citySet, countySet, townSet, distSet
	global w2freq, w2pos, w2brown, struct_lst
	para = set(sys.argv[1].split("_"))
	if "2" in para:
		loc_dict = set([l.strip() for l in codecs.open(sys.argv[2], 'r', encoding='utf-8')])
	if "7" in para:
		provSet = set([l.strip() for l in codecs.open(sys.argv[3], 'r', encoding='utf-8')])
		citySet = set([l.strip() for l in codecs.open(sys.argv[4], 'r', encoding='utf-8')])
		countySet = set([l.strip() for l in codecs.open(sys.argv[5], 'r', encoding='utf-8')])
		townSet = set([l.strip() for l in codecs.open(sys.argv[6], 'r', encoding='utf-8')])
		distSet = set([l.strip() for l in codecs.open(sys.argv[7], 'r', encoding='utf-8')])
	if "8" in para:
		tt, cnt, batch_num = 0, 0, 10
		pdict = re.compile(u"(\S+) \d+ (\S+),")
		dict_lst = [l.strip() for l in codecs.open(sys.argv[8], 'r', encoding='utf-8')]
		batch_size = len(dict_lst)/batch_num
		for l in dict_lst:
			if cnt == batch_size:
				cnt = 0
				tt += 1
			m = pdict.match(l)
			w = m.group(1)
			pos_lst = m.group(2).split(",")
			w2freq[w] = u"Group{0}".format(str(tt))
			w2pos[w] = pos_lst
			cnt += 1
		pbrown = re.compile(u"(\d+)\t(\S+)\t\d+\n")
		brown_f = codecs.open(sys.argv[9], 'r', encoding='utf-8')
		for l in brown_f:
			m = pbrown.match(l)
			w2brown[m.group(2)] = m.group(1)
		brown_f.close()
	if "9" in para:
		struct_lst = [l.strip() for l in codecs.open(sys.argv[10], 'r', encoding='utf-8')]
	for l in input_f:
		m = p.match(l)
		seg = m.group(1)
		case_lst = m.group(2).split(u",")
		ori = u"".join(seg.split())
		for case in case_lst:
			mc = pp.match(case)
			lb, abbr = mc.group(1), mc.group(2)
			ori_abbr= u"".join(abbr.split())
			fea_str = lb + u" qid:" + str(qid) + u" #" + ori + u":" + ori_abbr + u"#" + template(seg, abbr)
			if len(struct_lst):
				fea_str += " " + template_struct(struct_lst[qid-1], abbr)
			print fea_str
		qid += 1

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
