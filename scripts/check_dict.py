#! /usr/bin/python
# --------------------------------------- 
# File Name : check_dict.py
# Creation Date : 29-07-2014
# Last Modified : Tue Jul 29 16:15:58 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *

def proc(input_f):
	f2id = pickle.load(open(sys.argv[1], 'rb'))
	for f, fid in f2id.items():
		print u"{0}:{1}".format(f, str(fid))

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)

if __name__ == "__main__": main()
