#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Thu Aug  7 20:19:28 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random
from itertools import *
from operator import itemgetter

gram, thr = 4, 5 
digitS = u"\d|一|二|三|四|五|六|七|八|九|十"
numS = u"第?(?:(?:{0})+(?:百|千)*(?:万|亿)*)+号?".format(digitS)
numPtrn = re.compile(u"(?:(?:{0})+)".format(numS))
dirS = u"(?:(?:东|南|西|北|东北|东南|西北|西南)方?)".format(digitS)
dirPtrn = re.compile(dirS)
numDirS = u"{0}|{1}".format(numS, dirS)
numDirPtrn = re.compile(numDirS)
type_dict = {}

def segmentation(l, ptrn):
	itr = ptrn.finditer(l)
	s, seg_lst = 0, []
	for m in itr:
		ns = m.span()[0]
		suff = m.group()
		tmp_lst = segmentation_numdir(l[s:ns])
		if suff == u"中" or suff == u"小":
			if not len(tmp_lst) or not tmp_lst[len(tmp_lst)-1].endswith(u"/NUM"):
				continue
		tmp_lst += [u"{0}/{1}".format(suff, type_dict[m.group()])] 
		seg_lst += tmp_lst 
		s = m.span()[1] 
	if s != len(l):
		seg_lst += segmentation_numdir(l[s:len(l)])
	return seg_lst

def segmentation_numdir(l):
	itr = numDirPtrn.finditer(l)
	s, seg_lst = 0, []
	for m in itr:
		ns = m.span()[0]
		suff = m.group()
		if numPtrn.match(suff):
			tmp_lst = [u"{0}/NUM".format(suff)]
		if dirPtrn.match(suff):
			tmp_lst = [u"{0}/DIR".format(suff)]
		if s != ns:
			tmp_lst = [u"{0}/MISC".format(l[s:ns])] + tmp_lst 
		seg_lst += tmp_lst 
		s = m.span()[1] 
	if s != len(l):
		seg_lst += [u"{0}/MISC".format(l[s:len(l)])]
	return seg_lst


def proc(input_f):
	global gram, thr, digitS, numS, type_dict
	spec_loc_lst = []
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[1], 'r', encoding='utf-8')] #provS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[2], 'r', encoding='utf-8')] #cityS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[3], 'r', encoding='utf-8')] #cntyS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[4], 'r', encoding='utf-8')] #townS
	spec_loc_lst += [l.strip() for l in codecs.open(sys.argv[5], 'r', encoding='utf-8')] #distS
	spec_loc_lst.sort(key=len, reverse=True)
	street_lst = [u"街", u"弄", u"路", u"巷", u"胡同", u"里", u"大道"]
	agency_lst = [u"院", u"所", u"处", u"办", u"局", u"厅", u"厂", u"社", u"委"] #suff_agency
	sch_lst = [u"小学", u"学校", u"小", u"附小", u"校", u"小学校", u"高级中学",u"中学", u"高中", u"中", u"附中", u"医学院", u"学院", u"大学", u"师大"] #suff_univ
	res_lst = [u"新家园", u"家属楼", u"家属院", u"小区", u"花园", u"园", u"宿舍", u"大厦", u"公寓", u"家园", u"社区", u"新村", u"社区", u"广场", u"别墅", u"山庄", u"大楼", u"大院", u"中心", u"苑"] #suff_res
	hot_lst = [ u"大酒店", u"酒店", u"宾馆", u"公寓", u"饭店", u"连锁", u"旅馆"] #suff_hot
	misc_lst = []
#	type_lst = [(spec_loc_lst, "GEN"), (street_lst, "STR"), (agency_lst, "AGE"), (sch_lst, "SCH"), (res_lst, "RES"), (hot_lst, "HOT"), (misc_lst, "MISC")]
	type_lst = [(spec_loc_lst, "GEO"), (street_lst, "STR")]
	cand_lst, tag_lst = [], []
	for l in input_f:
		l = l.strip()
		tag_lst += [[l]]
		cand_lst += [l]
	for prfx_lst, ty in type_lst:
		prefix = u"|".join([u"\S+{0}".format(suff) for suff in prfx_lst])
		if ty == "GEN":
			prefix = u"|".join([u"{0}".format(suff) for suff in prfx_lst])
		prfxStr = u"((?:{0})*)(\S*?)$".format(prefix)
		prfxPtrn = re.compile(prfxStr)
		for i, item in enumerate(tag_lst):
			if item[0] == u"": continue
			m = prfxPtrn.match(item[0])
			if not len(m.group(1)): continue
			tag_lst[i][0] = m.group(2)
			tag_lst[i] += [u"{0}/{1}".format(m.group(1), ty)]
	kwd_lst = agency_lst + sch_lst + res_lst + hot_lst
	kwd_lst.sort(key=len, reverse=True)
	kwd_str = u"|".join(kwd_lst)
	kwdP = re.compile(kwd_str)
	for age in agency_lst: type_dict[age]="AGE"
	for sch in sch_lst: type_dict[sch]="SCH"
	for res in res_lst: type_dict[res]="RES"
	for hot in hot_lst: type_dict[hot]="HOT"
	for i, item in enumerate(tag_lst):
		if item[0] == u"": continue
		tag_lst[i] += segmentation(item[0], kwdP)
	for i in range(0, len(tag_lst)):
		print u"{0}:{1}".format(cand_lst[i], " ".join(tag_lst[i][1:len(tag_lst[i])]))

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
