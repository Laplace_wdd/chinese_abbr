#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : filter.py
# Creation Date : 22-07-2014
# Last Modified : Tue Jul 22 13:10:56 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *

def sat(abbr, cand):
	if u"." in abbr:
		return False
	if u"·" in abbr:
		return False
	if len(abbr) == 1:
		return False
	return True

def fltr(input_stream):
	for l in input_stream:
		arr = l.strip().split()
		q = arr[0]
		abbr_lst = arr[1:len(arr)]
		val_lst = [abbr for abbr in abbr_lst if sat(abbr, q)]
		print q + " " + " ".join(val_lst)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	fltr(input_stream)

if __name__ == "__main__": main()
