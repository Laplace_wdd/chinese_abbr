#! /usr/bin/python
# --------------------------------------- 
# File Name : recover.py
# Creation Date : 20-07-2014
# Last Modified : Wed Aug  6 09:40:13 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *

def proc(pred_f, input_f):
	p = re.compile(u"(\d) qid:(\d+) #(\S+):(\S+)#") 
	full, qid, res = u"", u"", []
	for pred, case in izip(pred_f, input_f):
		m = p.match(case)
		lb, c_qid, c_full, abbr = m.group(1), m.group(2), m.group(3), m.group(4)
		score = float(pred.strip())
		if qid == u"":
			qid = c_qid
			full = c_full
		if c_qid != qid:
			yield full, res  
			full, qid, res = c_full, c_qid, []
		res += [[score, lb, abbr]] 
	yield full, res 

def main():
	pred_f = codecs.open(sys.argv[1], 'r', encoding='utf-8') 
	input_f = codecs.open(sys.argv[2], 'r', encoding='utf-8') 
	out_f = codecs.open(sys.argv[3], 'w', encoding='utf-8') 
	topk, total, mp, mr, tmp, tmr, tv = int(sys.argv[4]), 0, 0.0, 0.0, 0.0, 0.0, 0.0
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	for full, info_list in proc(pred_f, input_f):
		info_list = sorted(info_list, reverse=True)
		info_list = [[1, u"2", full]] + info_list
		cnt, tp, tt, abbr_lst = 0, 0.0, 0.0, []
		total += 1
		for s, l, abbr in info_list:
			if l == u"2":
				if cnt < topk:
					tp += 1
				tt += 1
			if cnt < topk:
				abbr_lst += [abbr]
			cnt += 1
		mp += tp
		mr += tt
		dv = tt
		if dv > topk: dv = topk
		tv += dv
		tmp += tp/dv
		tmr += tp/tt
		print >> out_f, full + ":" + ",".join(abbr_lst)
	print "Macro Precision@" + str(topk) + "," + str(tmp/total)
	print "Macro Recall@" + str(topk) + "," + str(tmr/total)
	print "Micro Precision@" + str(topk) + "," + str(mp/tv)
	print "Micro Recall@" + str(topk) + "," + str(mp/mr)

if __name__ == "__main__": main()
