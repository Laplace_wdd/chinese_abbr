#! /usr/bin/python
# --------------------------------------- 
# File Name : viterbi_tagger.py
# Creation Date : 13-08-2014
# Last Modified : Fri Aug 15 16:51:47 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, time
from itertools import *
q, e, S, S_forbid = {}, {}, set(), set(["GEO", "NUM"])

#function for generating the tag set S, transition and emission
def load_dict(trans, emi):
	global q, e, S
	trans_f = codecs.open(trans, 'r', encoding='utf-8') 
	ptrn = re.compile('(\S+)\s(\S+)\s(\S+)')
	for l in trans_f:
		m = ptrn.match(l)
		q[m.group(1), m.group(2)] = float(m.group(3))
	trans_f.close()
	emission_f = codecs.open(emi, 'r', encoding='utf-8') 
	for l in emission_f:
		m = ptrn.match(l)
		word = m.group(2)
		tag = m.group(1)
		val = float(m.group(3))
		if word in e:
			e[word][tag] = val
		else:
			e[word] = {tag:val}
		S.add(tag)
	emission_f.close()

#the viterbi algorithm
#x: the input sentence
#y: the observed tags 
def viterbi(x, y):
	global q, e, S
	n = len(x)
	if n == 0: return u"" 
	#initialization for the DP
	pi = {}
	for v in S:
		s0 = float('-inf')
		if y[0] in S:
			if v == y[0]:
				pi[0,v] = (0, [])
			else:
				pi[0,v] = (s0, [])
		else:
			if (u"START", v) in q:
				s0 = q[u"START", v]
			pi[0,v] = (s0, [])
	for k in range(1, n):
		x_k, y_k = x[k], y[k]
		for v in S:
			max_prob, max_w, e_xv = (float('-inf'), []), u"", float('-inf')
			if x_k in e and v in e[x_k]: e_xv = e[x_k][v]
			for w in S:
				q_wv, pi_kw = float('-inf'), (float('-inf'), [])
				if (w,v) in q: q_wv = q[w,v]
				if (k-1,w) in pi: pi_kw = pi[k-1,w]
				tmp_prob = pi_kw[0] + q_wv + e_xv
				if tmp_prob > max_prob[0]:
					max_prob, max_w = (tmp_prob, pi_kw[1] + [w]), w
			if max_w != u"":
				if y_k in S:
					if y_k == v: pi[k, v] = (0, max_prob[1])
					else: pi[k, v] = (float('-inf'), max_prob[1])
				else:
					pi[k, v] = max_prob	
	max_prob = (float('-inf'), [])
	#find argmax_uv(pi(n-1,u,v)q('STOP'|u,v))
	for w in S:
		q_we, pi_kw = float('-inf'), (float('-inf'), [])
		if (w, "END") in q: q_we = q[w, "END"]
		if (n-1,w) in pi:
			pi_kw = pi[n-1,w]
			tmp_prob = pi_kw[0] + q_we 
			if tmp_prob > max_prob[0]:
				max_prob = (tmp_prob, pi_kw[1] + [w])
	tl = max_prob[1]
	#find the trace of the tags
	out = u" ".join([u"{0}/{1}".format(w, tl[i]) for i, w in enumerate(x)])
	return out

def proc(input_stream):
	ptrn = re.compile(u"(\S+)/(\S+)")
	for l in input_stream:
		l_slst = l.strip().split()
		wlst, tlst = [], []
		for itm in l_slst:
			m = ptrn.match(itm)
			wlst += [m.group(1)]
			if m.group(2) == "MISC" or m.group(2) == "FREQ":
				tlst += [""]
			else:
				S.add(m.group(2))
				tlst += [m.group(2)]
		print viterbi(wlst, tlst)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	load_dict(sys.argv[1], sys.argv[2])
	startT = time.clock()
	proc(input_stream)
	endT = time.clock()
	print >> sys.stderr, 'Processing Time:' + str((endT - startT)) + 'sec'

if __name__ == "__main__": main()
