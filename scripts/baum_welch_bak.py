#! /usr/bin/python
# --------------------------------------- 
# File Name : baum_welch.py
# Creation Date : 08-08-2014
# Last Modified : Sat Aug 16 08:48:09 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random, math, time
from itertools import *
#python fba.py ul_input.dat trans.dat emi.dat stat.dat 10000
S = set() #the state set
q = {} #transition prob
e = {} #emission prob
x, y = [], [] #input observation
alpha = {} #forward prob
beta = {} #backward prob
m = 0 #sentence length
e_n = {} #backup for e
q_n = {} #backup for q
likelihood = 0
ptrn = re.compile(u"(\S+)/(\S+)")
smooth = 0.01 
#function for generating the tag set S and the emission
def pi(u,v,j):
	if j < m and y[j] != "":
		if v != y[j]: return 0.0
		else: return 1.0
	tss = random.random() 
	if (u,v) in q:
		tss = q[u,v]
	else:
		q[u,v] = tss
	if j == m: return tss
	emi = random.random() 
	if (v, x[j]) in e:
		emi = e[v,x[j]]
	else:
		e[v,x[j]] = emi
	return tss*emi 

def forward():
	global alpha
	alpha = {}
	for s in S:
		alpha[0,s] = pi("START",s,0)
	for i in range(1, m):
		for v in S:
			base = 0
			for u in S:
				base = base + alpha[i-1,u]*pi(u,v,i)
			alpha[i,v] = base

def backward():
	global beta
	beta = {}
	for s in S:
		beta[m-1,s] = pi(s,"END", m) 
	for i in range(m-2,-1,-1):
		for v in S:
			base = 0
			for u in S:
				base = base + beta[i+1,u]*pi(v,u,i+1)
			beta[i,v] = base

def estep():
	global m
	global likelihood
	m = len(x)
	forward()
	backward()
	Z = 0
	mu = {}
	for s in S:
		Z = Z + alpha[m-1,s]
	likelihood = likelihood + math.log(Z)
	for j in range(0,m):
		for a in S:
			mu[j,a] = alpha[j,a]*beta[j,a]/Z
	for j in range(0,m-1):
		for a in S:
			for b in S:
				mu[j,a,b] = alpha[j,a]*pi(a,b,j+1)*beta[j+1,b]/Z
	for a in S:
		if "START" in q_n:
			if a in q_n["START"]:
				q_n["START"][a] = q_n["START"][a] + mu[0,a]
			else:
				q_n["START"][a] = mu[0,a]
		else:
			q_n["START"] = {a:mu[0,a]}
	for a in S:
		if a in q_n:
			if "END" in q_n[a]:
				q_n[a]["END"] = q_n[a]["END"] + mu[m-1,a]
			else:
				q_n[a]["END"] = mu[m-1,a]
		else:
			q_n[a] = {"END":mu[m-1,a]}
	for a in S:
		for b in S:
			nume = 0
			for i in range(0, m-1):
				nume = nume + mu[i,a,b]
			if a in q_n:
				q_n[a][b] = nume
			else:
				q_n[a] = {b:nume}
	for a in S:
		for i in range(0,m):
			if a in e_n:
				if x[i] in e_n[a]:
					e_n[a][x[i]] = e_n[a][x[i]] + mu[i,a] 
				else:
					e_n[a][x[i]] = mu[i,a]
			else:
				e_n[a] = {x[i]:mu[i,a]}

def	mstep():
	global e, q, e_n, q_n
	e, q = {}, {} 
	for a in q_n:
		base = 0
		for b in q_n[a]:
			base = base + q_n[a][b] + smooth 
		for b in q_n[a]:
			if base == 0: base = 1
			q[a,b] = (q_n[a][b] + smooth)/base
	for a in e_n:
		base = 0
		for w in e_n[a]:
			base = base + e_n[a][w] +smooth 
		for w in e_n[a]:
			if base == 0: base = 1
			e[a,w] = (e_n[a][w] + smooth)/base
	q_n, e_n = {}, {}

def genPara(trans_f, emission_f):
	for (u,v) in q:
		print >> trans_f, u"{0} {1} {2}".format(str(u), str(v), str(math.log(q[u,v])))
	for (v,w) in e:
		print >> emission_f, u"{0} {1} {2}".format(unicode(v), w, str(math.log(e[v,w])))

#arg1: log emission probability of words and tags	
#arg2: log trigram probability 
#arg3: the input set of sentences
#arg4: the result words with tages
#arg5: the type of replacement
def proc(input_stream):
	global x, y, likelihood, S
	passage = []	
	S = set()
	for l in input_stream:
		l_slst = l.strip().split()
		wlst, tlst = [], []
		for itm in l_slst:
			m = ptrn.match(itm)
			wlst += [m.group(1)]
			if m.group(2) == "MISC" or m.group(2) == "FREQ":
				tlst += [""]
			else:
				S.add(m.group(2))
				tlst += [m.group(2)]
		passage += [(wlst, tlst)]
	state_num, count_thr = int(sys.argv[3]), int(sys.argv[4])
	hidden_state = set(range(0, state_num))
	S |= hidden_state
	old_likelihood, itr, thr, sl, batch_size = 1, 0, 0.0001, 15, 100
	while abs(old_likelihood - likelihood) > thr and itr < count_thr:
		old_likelihood = likelihood
		likelihood = 0
		for wlst, tlst in passage:
			x, y = wlst, tlst
			estep()
		mstep()
		itr = itr + 1
		if not itr%batch_size:
			num = str(itr/batch_size)
			trans_f = codecs.open(sys.argv[1]+"."+num, 'w', encoding='utf-8') 
			emission_f = codecs.open(sys.argv[2]+"."+num, 'w', encoding='utf-8') 
			genPara(trans_f, emission_f)
		print >> sys.stderr, str(itr) + ' ' + str(likelihood)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	startT = time.clock()
	proc(input_stream)
	endT = time.clock()
	trans_f = codecs.open(sys.argv[1], 'w', encoding='utf-8') 
	emission_f = codecs.open(sys.argv[2], 'w', encoding='utf-8') 
	genPara(trans_f, emission_f)
	trans_f.close()
	emission_f.close()
	print >> sys.stderr, 'Processing Time:' + str((endT - startT)) + 'sec'

if __name__ == "__main__": main()
