#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Fri Aug  8 14:26:00 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random
from itertools import *
from operator import itemgetter

gram, thr = 4, 5 
geo_set, freq_set = set(), set()
street_set = set([u"街", u"弄", u"路", u"巷", u"胡同", u"里"])
agency_set = set([u"院", u"所", u"处", u"办", u"局", u"厅", u"厂", u"社", u"委"]) #suff_agency
sch_set = set([u"小学", u"学校", u"小", u"校", u"小学校", u"高级中学",u"中学", u"高中", u"中", u"医学院", u"学院", u"大学", u"师大"]) #suff_univ
res_set = set([u"新家园", u"家属楼", u"家属院", u"小区", u"花园", u"园", u"宿舍", u"大厦", u"公寓", u"家园", u"社区", u"新村", u"社区", u"广场", u"别墅", u"山庄", u"大楼", u"大院", u"中心", u"苑"]) #suff_res
hot_set = set([ u"大酒店", u"酒店", u"宾馆", u"公寓", u"饭店", u"连锁", u"旅馆"]) #suff_hot

digitS = u"\d|一|二|三|四|五|六|七|八|九|十"
numS = u"第?(?:(?:{0})+(?:百|千)*(?:万|亿)*)+号?".format(digitS)
numPtrn = re.compile(u"^(?:(?:{0})+)$".format(numS))
dirS = u"^(?:(?:东|南|西|北|东北|东南|西北|西南)方?)$".format(digitS)
dirPtrn = re.compile(dirS)
dp = {}

vp = re.compile(u"[ -~]+")
def valid(cand):
	res = []
	for c in cand:
		if vp.match(c) and len(res) and vp.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def parse_loc(loc_lst):
	global dp
	loc = u"".join(loc_lst)
	if loc in dp:
		return dp[loc]
	elif loc in geo_set:
		return ["GEO", 0, loc]
	elif loc in street_set:
		return ["STR", 1, loc]
	elif loc in agency_set:
		return ["AGE", 1, loc]
	elif loc in sch_set:
		return ["SCH", 1, loc]
	elif loc in res_set:
		return ["RES", 1, loc]
	elif loc in hot_set:
		return ["HOT", 1, loc]
	elif numPtrn.match(loc):
		return ["NUM", 1, loc]
	elif dirPtrn.match(loc):
		return ["DIR", 1, loc]
	elif loc in freq_set:
		return ["FREQ", 3, loc]
	elif len(loc_lst) == 1:
		return ["MISC", 4, loc]
	else:
		tr, score, res = "", 99999, [] 
		for k in range(1, len(loc_lst)):
			left = parse_loc(loc_lst[0:k])
			right = parse_loc(loc_lst[k:len(loc_lst)])
			t_score = left[1]+right[1]
			if t_score < score:
				score = t_score
				res = [right[0], score, [left, right]]
		dp[loc] = res
		return res

def tree2lst(tr):
	if type(tr[2]) is list:
		return tree2lst(tr[2][0]) + tree2lst(tr[2][1])
	return [u"{0}/{1}".format(tr[2],tr[0])]

def lst2str(seg_lst):
	res = []
	p=re.compile(u"(\S+)/(\S+)")
	state = 0
	for seg in seg_lst:
		m = p.match(seg)
		cha = m.group(1)
		tag = m.group(2)
		if tag == "MISC":
			if state:
				res[len(res)-1] += cha
			else:
				res += [cha]
			state = 1
		else:
			if state:
				res[len(res)-1] += "/MISC"
			res += [seg]
			state = 0
	if state:
		res[len(res)-1] += "/MISC"
	return u" ".join(res)

def proc(input_f):
	global geo_set, freq_set
	geo_lst = []
	geo_lst += [l.strip() for l in codecs.open(sys.argv[1], 'r', encoding='utf-8')]
	geo_lst += [l.strip() for l in codecs.open(sys.argv[2], 'r', encoding='utf-8')]
	geo_lst += [l.strip() for l in codecs.open(sys.argv[3], 'r', encoding='utf-8')]
	geo_lst += [l.strip() for l in codecs.open(sys.argv[4], 'r', encoding='utf-8')]
	geo_lst += [l.strip() for l in codecs.open(sys.argv[5], 'r', encoding='utf-8')]
	geo_set = set(geo_lst)
	pdict = re.compile(u"(\S+) \d+ (\S+),")
	freq_set = set([pdict.match(l.strip()).group(1) for l in codecs.open(sys.argv[6], 'r', encoding='utf-8')][0:1000])
	for l in input_f:
		ll = valid(l.strip())
		tag_lst = tree2lst(parse_loc(ll))
		print u"{0}:{1}".format(l.strip(), lst2str(tag_lst))

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
