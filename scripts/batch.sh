#/bin/bash
# --------------------------------------- 
# File Name : run.sh
# Creation Date : 19-07-2014
# Last Modified : Wed Aug  6 12:01:30 2014
# Created By : wdd 
# --------------------------------------- 
data_dir=../data
res_dir=${data_dir}/resource
raw_dir=${data_dir}/raw
svm_dir=../../tools/svm_rank
mod_dir=${data_dir}/model
out_dir=${data_dir}/out
#filter data, keep real abbreviation cases
for temp in 8_7_6_3.5_3_2_1 7_6_3.5_3_2_1 6_5_3.5_3_2_1 6_3_2_1
do 
	thr=5
	misc=temp_${temp}_thr_${thr}
	vec_dir=${data_dir}/vec/${temp}
	mkdir ${vec_dir}
	dict_lst="${data_dir}/loc.dict ${res_dir}/province.dict ${res_dir}/city.dict ${res_dir}/county.dict ${res_dir}/town.dict ${res_dir}/district.dict ${data_dir}/sougou.dict ${data_dir}/brown.dict"
	cat ${raw_dir}/trn.la | python gen_ftr_template.py ${temp} ${dict_lst} > ${vec_dir}/trn.temp
	cat ${raw_dir}/tst.la | python gen_ftr_template.py ${temp} ${dict_lst} > ${vec_dir}/tst.temp
	cat ${raw_dir}/dev.la | python gen_ftr_template.py ${temp} ${dict_lst} > ${vec_dir}/dev.temp
	#generate feature vector from the templates
	cat ${vec_dir}/trn.temp | python gen_ftr_dict.py ${vec_dir}/ftr2id_thr${thr}.pkl ${vec_dir}/ftr2freq.csv ${vec_dir}/ftr2id_thr${thr}.csv ${thr} 
	cat ${vec_dir}/trn.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/trn_thr${thr}.vec
	cat ${vec_dir}/tst.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/tst_thr${thr}.vec
	cat ${vec_dir}/dev.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/dev_thr${thr}.vec
	eps=0.5
	for C in 0.005
	do
		conf=c${C}e${eps}_${misc}
		mkdir ${out_dir}/${conf}
		echo "" > ${out_dir}/${conf}/log_svm.txt
		echo "" > ${out_dir}/${conf}/log.csv
		${svm_dir}/svm_rank_learn -c ${C} -e ${eps} ${vec_dir}/trn_thr${thr}.vec ${mod_dir}/${conf}.model
		${svm_dir}/svm_rank_classify ${vec_dir}/dev_thr${thr}.vec ${mod_dir}/${conf}.model ${out_dir}/${conf}/dev.out >> ${out_dir}/${conf}/log_svm.txt 
		${svm_dir}/svm_rank_classify ${vec_dir}/tst_thr${thr}.vec ${mod_dir}/${conf}.model ${out_dir}/${conf}/tst.out >> ${out_dir}/${conf}/log_svm.txt 
		for topK in 5 20
		do
			echo "dev result" >> ${out_dir}/${conf}/log.csv  
			python recover_rank.py ${out_dir}/${conf}/dev.out  ${vec_dir}/dev.temp ${out_dir}/${conf}/dev.cand${topK} ${topK} >> ${out_dir}/${conf}/log.csv 
			echo "test result" >> ${out_dir}/${conf}/log.csv  
			python recover_rank.py ${out_dir}/${conf}/tst.out  ${vec_dir}/tst.temp ${out_dir}/${conf}/tst.cand${topK} ${topK} >> ${out_dir}/${conf}/log.csv 
		done
	done
done
