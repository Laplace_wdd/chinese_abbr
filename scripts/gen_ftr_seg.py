#! /usr/bin/python
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Wed Jul 23 18:07:53 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *

word2vec, bos_v, eos_v, oov_v, num_v, pu_v, eng_v = {}, [], [], [], [], [], []

def load_dict():
	global word2vec, bos_v, eos_v, oov_v, num_v, pu_v, eng_v 
	dict_f = codecs.open(sys.argv[1], 'r', encoding='utf-8')
	for l in dict_f:
		fea_lst = l.split()
		if fea_lst[0] == u"<BOS>":
			bos_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<EOS>":
			eos_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<OOV>":
			oov_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<NUM>":
			num_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<PU>":
			pu_v = fea_lst[1:len(fea_lst)]
		elif fea_lst[0] == u"<ENG>":
			eng_v = fea_lst[1:len(fea_lst)]
		else:
			word2vec[fea_lst[0]] = fea_lst[1:len(fea_lst)]
	dict_f.close()
	
p = re.compile(u"[ -~]+")

def valid(cand):
	res = []
	for c in cand:
		if p.match(c) and len(res) and p.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def decode(line):
	arr, res = valid(line.split()), []
	for word in arr:
		if p.match(word) or len(word) == 1:
			res += [(word, word, 1, 0, 1, 1, 1)]
		else:
			for i in range(0, len(word)):
				if i == 0:
					res += [(word[i], word, len(word), i, 1, 0, 0)]
				elif i == len(word)-1:
					res += [(word[i], word, len(word), i, 0, 1, 0)]
				else:
					res += [(word[i], word, len(word), i, 0, 0, 0)]
	return res

def get_vec(token):
	vec = []
	global word2vec, bos_v, eos_v, oov_v, num_v, pu_v, eng_v 
	if token in word2vec:
			vec = word2vec[token]
	elif re.compile(u"[A-Za-z]+").match(token):
		vec = eng_v
	elif re.compile(u"[0-9]+").match(token):
		vec = num_v
	elif re.compile(u"\W").match(token):
		vec = pu_v
	else:
		vec = oov_v
	return vec

def proc(input_f):
	seg_f = codecs.open(sys.argv[2], 'r', encoding='utf-8') 
	idx, pos, prev_fc, prev_case, pattern = 1, 0, 0, 0, []
	p = re.compile(u"(\d qid:(\d+) )fid:(\d+) (\S+)") 
	for l in input_f:
		m = p.match(l)
		cur_word, cur_fc, cur_case, cur_tt = m.group(4), m.group(3), m.group(2), m.group(1)
		if cur_fc != prev_fc:
			pattern = decode(seg_f.readline())
			prev_fc = cur_fc
		if cur_case != prev_case:
			pos = 0
			prev_case = cur_case
		cha, seg, seg_len, seg_pos, is_head, is_end, is_chara = pattern[pos]
		if cha != cur_word:
			print >> sys.stderr, cur_case + u" " + cur_fc + u" " + cha + u" " + cur_word
		seg_vec = get_vec(seg)
		idx = 1
		print cur_tt + " ".join([str(idx+i) + ":" + seg_vec[i] for i in range(0, len(seg_vec))]),
		idx += len(seg_vec)
		print str(idx) + ":" + str(seg_len),
		print str(idx+1) + ":" + str(seg_pos),
		print str(idx+2) + ":" + str(is_head),
		print str(idx+3) + ":" + str(is_end),
		print str(idx+4) + ":" + str(is_chara)
		idx += 4
		pos += 1
	seg_f.close()

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	load_dict()
	proc(input_stream)
	
if __name__ == "__main__": main()
