#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Sun Aug 10 15:17:52 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random
from itertools import *
from operator import itemgetter
type_dict, tag_set, forbid_tag = {}, set(), set([u"START", u"END", u"GEO", u"STR", u"NUM", u"DIR"])

def proc_group(input_f):
	old_lb, group_lb, rk_lst = u"", u"", []
	pdict = re.compile(u"(\S+)\t(\S+)\t(\S+)")
	for l in input_f:
		m = pdict.match(l)
		lb, wd, f = m.group(1), m.group(2), int(m.group(3))
		if old_lb == u"":
			old_lb = lb
			group_lb = lb
		if wd in tag_set:
			group_lb = wd
		if lb != old_lb:
			yield group_lb, sorted(rk_lst, key=itemgetter(1), reverse=True)
			old_lb, group_lb, rk_lst = lb, lb, []
		if len(wd) > 1:
			rk_lst += [(wd, f)]
	yield group_lb, sorted(rk_lst, key=itemgetter(1), reverse=True)

def proc(input_f):
	global type_dict, act_tag, forbid_tag
	pdict = re.compile(u"(\S+) \d+ (\S+),")
	freq_set = set([pdict.match(l.strip()).group(1) for l in codecs.open(sys.argv[1], 'r', encoding='utf-8')][0:1000])
	pdict = re.compile(u"(\S+)::(\S+)")
	for l in codecs.open(sys.argv[2], 'r', encoding='utf-8'): 
		m = pdict.match(l)
		type_dict[m.group(1)] = m.group(2)
		tag_set.add(m.group(2))
	for lb, rklst in proc_group(input_f):
		if lb in forbid_tag: continue
		for w, f in rklst:
			type_dict[w] = lb
			if f < 5: break
	for w, lb in type_dict.iteritems():
		print u"{0}::{1}".format(w, lb)
		
def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
