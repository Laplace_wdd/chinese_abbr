#! /usr/bin/python
# --------------------------------------- 
# File Name : gen_cand.py
# Creation Date : 21-07-2014
# Last Modified : Fri Jul 25 14:53:35 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs 
from itertools import *

vp = re.compile(u"[ -~]+")

def valid(cand):
	res = []
	for c in cand:
		if vp.match(c) and len(res) and vp.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def dfs(cand, i, abbr):
	if i == len(cand):
		if len(abbr) and len(abbr) < 11 and not len(abbr) == len(cand):
			sys.stdout.write(u" ".join(abbr) + u",")
	else:
		dfs(cand, i+1, abbr)
		dfs(cand, i+1, abbr + [cand[i]])

def genCand(input_f):
	qlen = [] 
	for l in input_f:
		l = l.strip()
		dfs(valid(l), 0, []) 
		print

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	genCand(input_stream)

if __name__ == "__main__": main()
