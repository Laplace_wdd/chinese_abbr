#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_ftr_seg.py
# Creation Date : 22-07-2014
# Last Modified : Sun Aug 10 18:13:43 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs, random, math
from itertools import *
from operator import itemgetter

gram, thr = 4, 5 
geo_set, freq_set = set(), set()
digitS = u"\d|一|二|三|四|五|六|七|八|九|十"
numS = u"第?(?:(?:{0})+(?:百|千)*(?:万|亿)*)+号?".format(digitS)
numPtrn = re.compile(u"^(?:(?:{0})+)$".format(numS))
dirS = u"^(?:(?:东|南|西|北|东北|东南|西北|西南)方?)$".format(digitS)
dirPtrn = re.compile(dirS)
dp, type_dict, trans_dict = {}, {}, {}

vp = re.compile(u"[ -~]+")
def valid(cand):
	res = []
	for c in cand:
		if vp.match(c) and len(res) and vp.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def parse_loc(loc_lst):
	global dp
	loc = u"".join(loc_lst)
	if loc in dp:
		return dp[loc]
	elif loc == u"#START#": return ["START", 0, loc]
	elif loc == u"#END#": return ["END", 0, loc]
	elif loc in type_dict:
		tag = type_dict[loc]
		if tag == "GEO": return ["GEO", 0, loc]
		else:
			return [tag, 1, loc]
	elif numPtrn.match(loc):
		return ["NUM", 1, loc]
#	elif dirPtrn.match(loc):
#		return ["DIR", 1, loc]
	elif loc in freq_set:
		return ["FREQ", 3, loc]
	elif len(loc_lst) == 1:
		return ["MISC", 4, loc]
	else:
		tr, score, res = "", 99999, [] 
		for k in range(1, len(loc_lst)):
			left = parse_loc(loc_lst[0:k])
			right = parse_loc(loc_lst[k:len(loc_lst)])
			t_score = left[1]+right[1]
			if (left[0], right[0]) in trans_dict:
				t_score *= trans_dict[left[0], right[0]] 
			if t_score < score:
				score = t_score
				res = [right[0], score, [left, right]]
		dp[loc] = res
		return res

def tree2lst(tr):
	if type(tr[2]) is list:
		return tree2lst(tr[2][0]) + tree2lst(tr[2][1])
	return [u"{0}/{1}".format(tr[2],tr[0])]

def lst2str(seg_lst):
	res = []
	p=re.compile(u"(\S+)/(\S+)")
	state = 0
	for seg in seg_lst:
		m = p.match(seg)
		cha = m.group(1)
		tag = m.group(2)
		if tag == "MISC":
			if state:
				res[len(res)-1] += cha
			else:
				res += [cha]
			state = 1
		else:
			if state:
				res[len(res)-1] += "/MISC"
			res += [seg]
			state = 0
	if state:
		res[len(res)-1] += "/MISC"
	return u" ".join(res)

def proc(input_f):
	global geo_set, type_dict, freq_set, trans_dict
	pdict = re.compile(u"(\S+) \d+ (\S+),")
	freq_set = set([pdict.match(l.strip()).group(1) for l in codecs.open(sys.argv[1], 'r', encoding='utf-8')][0:1000])
	pdict = re.compile(u"(\S+)::(\S+)")
	for l in codecs.open(sys.argv[2], 'r', encoding='utf-8'): 
		m = pdict.match(l)
		type_dict[m.group(1)] = m.group(2)
	pdict = re.compile(u"(\S+)\t(\S+)\t(\S+)")
	for l in codecs.open(sys.argv[3], 'r', encoding='utf-8'): 
		m = pdict.match(l)
		w0, w1, score = m.group(2), m.group(3), math.exp(-float(m.group(1)))
		trans_dict[w0, w1] = score 
	for l in input_f:
		ll = valid(l.strip().split())
		ll = [u"#START#"] + ll + [u"#END#"]
		tag_lst = tree2lst(parse_loc(ll))
		print u"{0}:{1}".format(l.strip(), lst2str(tag_lst))

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
