#! /usr/bin/python
# --------------------------------------- 
# File Name : gen_ftr_dict.py
# Creation Date : 25-07-2014
# Last Modified : Thu Jul 31 10:23:12 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *
from operator import itemgetter
def proc(input_f):
	thr = int(sys.argv[4])
	p, pt = re.compile(u"\d qid:\d+ #\S+:\S+#(.*)\n"), re.compile(u"<([^:]+)") 
	res, f2f, num = {}, {}, 1
	for l in input_f:
		m = p.match(l)
		ftr_lst = m.group(1).split()
		for f in ftr_lst:
			if f in f2f:
				f2f[f] += 1
			else:
				f2f[f] = 1
	for f in f2f:
		mpt = pt.match(f)
		f = u"<{0}:<OOV>>".format(mpt.group(1))
		if f in res: continue
		else:
			res[f] = num 
			num += 1
	f2f_f = codecs.open(sys.argv[2], 'w', encoding='utf-8') 
	for f, freq in sorted(f2f.items(), key=itemgetter(1), reverse = True):
		print >> f2f_f, u"{0},{1}".format(f, freq)
		if freq < thr: continue
		if f in res: continue
		else:
			res[f] = num 
			num += 1
	f2f_f.close()
	res["<OOV>"] = num
	pickle.dump(res, open(sys.argv[1], 'wb'))
	dict_f = codecs.open(sys.argv[3], 'w', encoding='utf-8') 
	for f, fid in res.items():
		print >> dict_f, u"{0},{1}".format(f, str(fid))
	dict_f.close()

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
