#! /usr/bin/python
# --------------------------------------- 
# File Name : cat_ftr.py
# Creation Date : 23-07-2014
# Last Modified : Wed Jul 23 21:38:42 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, io
from itertools import *

def cat(f1, f2):
	p = re.compile("(\d+ qid:\d+ )(.*)\n") 
	for l1, l2 in izip(f1, f2):
		if l1 == "\n": continue 
		m = p.match(l1)
		tt = m.group(1)
		ftr_lst = m.group(2).split()
		m = p.match(l2)
		ftr_lst += m.group(2).split()
		res_ftr = [re.compile("\d+:(\S+)").match(f).group(1) for f in ftr_lst]
		print tt + " ".join([str(i+1) + ":" + res_ftr[i] for i in range(0, len(res_ftr))])

def main():
	f1 = open(sys.argv[1], 'r') 
	f2 = open(sys.argv[2], 'r') 
	cat(f1, f2)
	f1.close()
	f2.close()

if __name__ == "__main__": main()
