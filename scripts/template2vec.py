#! /usr/bin/python
# --------------------------------------- 
# File Name : gen_ftr_dict.py
# Creation Date : 25-07-2014
# Last Modified : Tue Jul 29 17:22:12 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *

def proc(input_f):
	p, pt = re.compile(u"(\d qid:\d+ )#\S+:\S+#(.*)\n"), re.compile(u"<([^:]+)") 
	f2id = pickle.load(open(sys.argv[1], 'rb'))
	res, num = {}, 1
	for l in input_f:
		m = p.match(l)
		ftr_lst = m.group(2).split()
		bow_lst, f2v = [], {}
		for ftr in ftr_lst:
			fid = 0
			if ftr not in f2id:
				mpt = pt.match(ftr)
				ftr = u"<{0}:<OOV>>".format(mpt.group(1))
			fid = f2id[ftr]
			if fid in f2v:
				f2v[fid] += 1
			else:
				f2v[fid] = 1
		for fid in f2v:
			bow_lst += [(fid, f2v[fid])]
		bow_lst = sorted(bow_lst) 
		print m.group(1) + " ".join([str(fid) + ":" + str(value) for fid, value in bow_lst]) 
def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
