#/bin/bash
# --------------------------------------- 
# File Name : run.sh
# Creation Date : 19-07-2014
# Last Modified : Mon Aug 11 14:26:44 2014
# Created By : wdd 
# --------------------------------------- 

#filter data, keep real abbreviation cases
temp=9.5_9_6_3.5_3_2_1
thr=5
misc=temp_${temp}_thr_${thr}
data_dir=../data
raw_dir=${data_dir}/raw
seq_dir=${data_dir}/seq
vec_dir=${data_dir}/vec/${temp}
svm_dir=../../tools/svm_rank
mod_dir=${data_dir}/model
out_dir=${data_dir}/out
res_dir=${data_dir}/resource
#mkdir ${raw_dir} 
##cat ../data/label/done.csv | python gen_data.py > ${raw_dir}/abbr.txt
#cat ../data/label/done.csv | python gen_data_lab.py > ${raw_dir}/abbr.txt
##random suffle and split data to /train/test/development/ by 0.8/0.1/0.1
#cat ${raw_dir}/abbr.txt | python split.py ${raw_dir}/trn.txt ${raw_dir}/tst.txt ${raw_dir}/dev.txt
##generate tagged sequences
#mkdir ${seq_dir} 
#cat ${raw_dir}/trn.txt | python gen_trn.py > ${seq_dir}/trn.seq
#cat ${raw_dir}/tst.txt | python gen_tst.py > ${seq_dir}/tst.seq
#cat ${raw_dir}/dev.txt | python gen_tst.py > ${seq_dir}/dev.seq
#generate vectors for SVM
#mkdir ${vec_dir}
#cat ${seq_dir}/trn.seq | python gen_ftr_lex.py ${data_dir}/word_vectors_with_oov_300.txt > ${vec_dir}/trn_thr${thr}.vec
#cat ${seq_dir}/tst.seq | python gen_ftr_lex.py ${data_dir}/word_vectors_with_oov_300.txt > ${vec_dir}/tst_thr${thr}.vec
#cat ${seq_dir}/dev.seq | python gen_ftr_lex.py ${data_dir}/word_vectors_with_oov_300.txt > ${vec_dir}/dev_thr${thr}.vec
#scws -i ${raw_dir}/trn.full -o ${raw_dir}/trn.seg -c utf-8 -d ${data_dir}/dict.xdb  
#scws -i ${raw_dir}/tst.full -o ${raw_dir}/tst.seg -c utf-8 -d ${data_dir}/dict.xdb  
#scws -i ${raw_dir}/dev.full -o ${raw_dir}/dev.seg -c utf-8 -d ${data_dir}/dict.xdb  
#cat ${seq_dir}/trn.seq | python gen_ftr_seg.py ${data_dir}/word_vectors_with_oov_300.txt ${raw_dir}/trn.seg > ${vec_dir}/trn_thr${thr}.vec
#cat ${seq_dir}/tst.seq | python gen_ftr_seg.py ${data_dir}/word_vectors_with_oov_300.txt ${raw_dir}/tst.seg > ${vec_dir}/tst_thr${thr}.vec
#cat ${seq_dir}/dev.seq | python gen_ftr_seg.py ${data_dir}/word_vectors_with_oov_300.txt ${raw_dir}/dev.seg > ${vec_dir}/dev_thr${thr}.vec
#python cat_ftr.py ${data_dir}/vec_lex_m10p1/trn_thr${thr}.vec ${data_dir}/vec_seg_0/trn_thr${thr}.vec > ${vec_dir}/trn_thr${thr}.vec
#python cat_ftr.py ${data_dir}/vec_lex_m10p1/tst_thr${thr}.vec ${data_dir}/vec_seg_0/tst_thr${thr}.vec > ${vec_dir}/tst_thr${thr}.vec
#python cat_ftr.py ${data_dir}/vec_lex_m10p1/dev_thr${thr}.vec ${data_dir}/vec_seg_0/dev_thr${thr}.vec > ${vec_dir}/dev_thr${thr}.vec
###svm hmm
#mkdir ${mod_dir}
#mkdir ${out_dir}
#${svm_dir}/svm_hmm_learn -c ${C} -e ${eps} ${vec_dir}/trn_thr${thr}.vec ${mod_dir}/${conf}.model
#mkdir ${out_dir}/${conf}
#${svm_dir}/svm_hmm_classify ${vec_dir}/tst_thr${thr}.vec ${mod_dir}/${conf}.model ${out_dir}/${conf}/tst.out > ${out_dir}/${conf}/log.txt 
#${svm_dir}/svm_hmm_classify ${vec_dir}/dev_thr${thr}.vec ${mod_dir}/${conf}.model ${out_dir}/${conf}/dev.out >> ${out_dir}/${conf}/log.txt 
#recover abbreviation
#python recover.py ${out_dir}/${conf}/dev.out ${seq_dir}/dev.seq > ${out_dir}/${conf}/dev.res 
#python recover.py ${out_dir}/${conf}/tst.out ${seq_dir}/tst.seq > ${out_dir}/${conf}/tst.res 

##enumerate all candidate by DFS
#cat ${raw_dir}/trn.full | python gen_cand.py > ${raw_dir}/trn.cand
#cat ${raw_dir}/tst.full | python gen_cand.py > ${raw_dir}/tst.cand
#cat ${raw_dir}/dev.full | python gen_cand.py > ${raw_dir}/dev.cand
##add negative cases into training && testing
#python enlarge.py ${raw_dir}/trn.cand ${raw_dir}/trn.seg ${raw_dir}/trn.txt --train > ${raw_dir}/trn.la
#python enlarge.py ${raw_dir}/tst.cand ${raw_dir}/tst.seg ${raw_dir}/tst.txt --test > ${raw_dir}/tst.la
#python enlarge.py ${raw_dir}/dev.cand ${raw_dir}/dev.seg ${raw_dir}/dev.txt --test > ${raw_dir}/dev.la
##generate feature template
mkdir ${vec_dir}
dict_lst="${data_dir}/loc.dict ${res_dir}/province.dict ${res_dir}/city.dict ${res_dir}/county.dict ${res_dir}/town.dict ${res_dir}/district.dict ${data_dir}/sougou.dict ${data_dir}/brown.dict"
cat ${raw_dir}/trn.la | python gen_ftr_template.py ${temp} ${dict_lst} ${raw_dir}/trn.struct > ${vec_dir}/trn.temp
cat ${raw_dir}/tst.la | python gen_ftr_template.py ${temp} ${dict_lst} ${raw_dir}/tst.struct > ${vec_dir}/tst.temp
cat ${raw_dir}/dev.la | python gen_ftr_template.py ${temp} ${dict_lst} ${raw_dir}/dev.struct > ${vec_dir}/dev.temp
#generate feature vector from the templates
cat ${vec_dir}/trn.temp | python gen_ftr_dict.py ${vec_dir}/ftr2id_thr${thr}.pkl ${vec_dir}/ftr2freq.csv ${vec_dir}/ftr2id_thr${thr}.csv ${thr} 
cat ${vec_dir}/trn.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/trn_thr${thr}.vec
cat ${vec_dir}/tst.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/tst_thr${thr}.vec
cat ${vec_dir}/dev.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/dev_thr${thr}.vec
eps=0.5
##C=5
##for C in 0.005 0.05 0.5 5 50 500
##for C in 500 50 5 0.5 0.05 0.005 0.0005 0.00005 0.000005 0.0000005 
for C in 0.005
do
	conf=c${C}e${eps}_${misc}
	mkdir ${out_dir}/${conf}
	echo "" > ${out_dir}/${conf}/log_svm.txt
	echo "" > ${out_dir}/${conf}/log.csv
	${svm_dir}/svm_rank_learn -c ${C} -e ${eps} ${vec_dir}/trn_thr${thr}.vec ${mod_dir}/${conf}.model
	${svm_dir}/svm_rank_classify ${vec_dir}/dev_thr${thr}.vec ${mod_dir}/${conf}.model ${out_dir}/${conf}/dev.out >> ${out_dir}/${conf}/log_svm.txt 
	${svm_dir}/svm_rank_classify ${vec_dir}/tst_thr${thr}.vec ${mod_dir}/${conf}.model ${out_dir}/${conf}/tst.out >> ${out_dir}/${conf}/log_svm.txt 
	for topK in 5 20
	do
		echo "dev result" >> ${out_dir}/${conf}/log.csv  
		python recover_rank.py ${out_dir}/${conf}/dev.out  ${vec_dir}/dev.temp ${out_dir}/${conf}/dev.cand${topK} ${topK} >> ${out_dir}/${conf}/log.csv 
		echo "test result" >> ${out_dir}/${conf}/log.csv  
		python recover_rank.py ${out_dir}/${conf}/tst.out  ${vec_dir}/tst.temp ${out_dir}/${conf}/tst.cand${topK} ${topK} >> ${out_dir}/${conf}/log.csv 
	done
done
###cat ${raw_dir}/trn.la | python gen_gold.py > ${raw_dir}/trn.gold
###cat ${raw_dir}/tst.la | python gen_gold.py > ${raw_dir}/tst.gold
###cat ${raw_dir}/dev.la | python gen_gold.py > ${raw_dir}/dev.gold
#dict_lst="${res_dir}/province.dict ${res_dir}/city.dict ${res_dir}/county.dict ${res_dir}/town.dict ${res_dir}/district.dict"
#cat ${raw_dir}/trn.full | python brand_name_extractor.py ${dict_lst} #> trn.brand
#dict_lst="${res_dir}/province.dict ${res_dir}/city.dict ${res_dir}/county.dict ${res_dir}/town.dict ${res_dir}/district.dict"
#cat ${raw_dir}/trn.full | python auto_bn.py ${dict_lst} #> trn.brand
#cat ${raw_dir}/trn.full | python linear_bn.py ${dict_lst} #> trn.brand
#dict_lst="${res_dir}/province.dict ${res_dir}/city.dict ${res_dir}/county.dict ${res_dir}/town.dict ${res_dir}/district.dict ${data_dir}/sougou.dict"
#cat ${raw_dir}/trn.full | python parse_bn.py ${dict_lst} > ${data_dir}/try/trn_nodict.brand
#cat ${data_dir}/try/trn_nodict.brand | python replace.py > ${data_dir}/try/trn_nodict.sent
#dict_lst="${res_dir}/province.dict ${res_dir}/city.dict ${res_dir}/county.dict ${res_dir}/town.dict ${res_dir}/district.dict ${data_dir}/sougou.dict"
#python parse_bn_v2.py ${dict_lst} > ${res_dir}/type_dict.txt
