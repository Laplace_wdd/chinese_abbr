#! /usr/bin/python
# --------------------------------------- 
# File Name : gen_ftr_dict.py
# Creation Date : 25-07-2014
# Last Modified : Wed Aug  6 19:05:40 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *
from operator import itemgetter

#def proc(input_f):
#	f2f = {}
#	for l in input_f:
#		ftr_lst = l.strip().split()
#		if len(ftr_lst) > 0:
#			f = ftr_lst[len(ftr_lst) - 1]
##		for f in ftr_lst:
#			if f in f2f:
#				f2f[f] += 1
#			else:
#				f2f[f] = 1
#	for f, freq in sorted(f2f.items(), key=itemgetter(1), reverse = True):
#		print u"{0},{1}".format(f, freq)
vp = re.compile(u"[ -~]+")

def valid(cand):
	res = []
	for c in cand:
		if vp.match(c) and len(res) and vp.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def proc(input_f):
	f2f = {}
	for l in input_f:
		root_lst = valid(l.strip())
		for k in range(0,4):
			for i in range(0, len(root_lst) - k):
				word = u"".join(root_lst[i:i+k+1])
				if word in f2f:
					f2f[word] += 1
				else:
					f2f[word] = 1
	for f, freq in sorted(f2f.items(), key=itemgetter(1), reverse = True):
		print u"{0}GRAM##{1},{2}".format(str(len(f)), f, freq)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(input_stream)
	
if __name__ == "__main__": main()
