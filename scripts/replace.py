#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : filter.py
# Creation Date : 22-07-2014
# Last Modified : Fri Aug  8 20:51:27 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *

def rplc(input_stream):
	p, pp = re.compile(u".*:(.*)\n"), re.compile(u"(.*)/(.*)")
	for l in input_stream:
		m = p.match(l)
		lst = m.group(1).split()
		res_lst = []
		for itm in lst:
			mp = pp.match(itm)
			if mp.group(2) == "MISC" or mp.group(2) == "FREQ":
				res_lst += [mp.group(1)]
			else:
				res_lst += [mp.group(2)]
		print u" ".join(res_lst)

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	rplc(input_stream)

if __name__ == "__main__": main()
