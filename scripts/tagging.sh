#/bin/bash
# --------------------------------------- 
# File Name : run.sh
# Creation Date : 19-07-2014
# Last Modified : Tue Aug 12 11:39:58 2014
# Created By : wdd 
# --------------------------------------- 

data_dir=../data
res_dir=${data_dir}/resource
raw_dir=${data_dir}/raw
brown_dir=${data_dir}/brown
brown=/Users/dqwang/Study/research/mobvoi/synonym/tools/brown
data_set=all_unlabeled
clus=12
cp ${res_dir}/type_dict.txt ${brown_dir}/type_dict.txt
touch ${brown_dir}/collocs
for i in `seq 1 30`;
do
	#tag the training data using the current dictionary and transition probability
	cat ${data_dir}/unlabeled/${data_set}.seg | python parse_bn_v2.py ${data_dir}/sougou.dict ${brown_dir}/type_dict.txt ${brown_dir}/collocs > ${brown_dir}/${data_set}.tagged.$i
	#replace the tagged word by its tag
	cat ${brown_dir}/${data_set}.tagged.$i | python replace.py > ${brown_dir}/${data_set}.$i 
	#brown clustering that generate word2cluster map and words transition probability
	${brown}/wcluster --text ${brown_dir}/${data_set}.$i --c ${clus} --ncollocs 1000
	mv ${data_set}-c${clus}-p1.out ${brown_dir}/${sent}$i-c${clus}-p1.out
	cp ${brown_dir}/type_dict.txt ${brown_dir}/type_dict_$i.txt 
	cp ${brown_dir}/${data_set}$i-c${clus}-p1.out/collocs ${brown_dir}/collocs 
	cp ${brown_dir}/${data_set}$i-c${clus}-p1.out/paths ${brown_dir}/paths 
	#update the dictionary by clustering result
	cat ${brown_dir}/${data_set}$i-c${clus}-p1.out/paths | python gen_type_dict.py ${data_dir}/sougou.dict ${brown_dir}/type_dict_$i.txt > ${brown_dir}/type_dict.txt
done
#tag the target data set using the current model
cat  ${raw_dir}/trn.la | python decode.py ${brown_dir}/type_dict.txt ${brown_dir}/paths ${brown_dir}/collocs > ${raw_dir}/trn.struct
cat  ${raw_dir}/dev.la | python decode.py ${brown_dir}/type_dict.txt ${brown_dir}/paths ${brown_dir}/collocs > ${raw_dir}/dev.struct
cat  ${raw_dir}/tst.la | python decode.py ${brown_dir}/type_dict.txt ${brown_dir}/paths ${brown_dir}/collocs > ${raw_dir}/tst.struct
