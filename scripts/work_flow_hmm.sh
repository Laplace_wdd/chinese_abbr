#/bin/bash
# --------------------------------------- 
# File Name : run.sh
# Creation Date : 19-07-2014
# Last Modified : Fri Aug 15 23:33:49 2014
# Created By : wdd 
# --------------------------------------- 

#filter data, keep real abbreviation cases
temp=9.5_9_6_3.5_3_2_1
thr=5
misc=hmm_temp_${temp}_thr_${thr}
data_dir=../data
raw_dir=${data_dir}/raw
vec_dir=${data_dir}/vec/${temp}
svm_dir=../../tools/svm_rank
mod_dir=${data_dir}/model
out_dir=${data_dir}/out
res_dir=${data_dir}/resource
work_dir=${data_dir}/plhmm
#mkdir ${raw_dir} 
##convert labeled to better form
#cat ../data/labeled/done.csv | python gen_data_lab.py > ${raw_dir}/abbr.txt
##random suffle and split data to /train/test/development/ by 0.8/0.1/0.1
#cat ${raw_dir}/abbr.txt | python split.py ${raw_dir}/trn.txt ${raw_dir}/tst.txt ${raw_dir}/dev.txt
##generate file for full case only
#cat trn.txt | perl -n -e '/(.*):/ && print "$1\n"' > trn.full
#cat tst.txt | perl -n -e '/(.*):/ && print "$1\n"' > tst.full
#cat dev.txt | perl -n -e '/(.*):/ && print "$1\n"' > dev.full
##word segmentation using scws
#scws -i ${raw_dir}/trn.full -o ${raw_dir}/trn.seg -c utf-8 -d ${data_dir}/dict.xdb  
#scws -i ${raw_dir}/tst.full -o ${raw_dir}/tst.seg -c utf-8 -d ${data_dir}/dict.xdb  
#scws -i ${raw_dir}/dev.full -o ${raw_dir}/dev.seg -c utf-8 -d ${data_dir}/dict.xdb  
##enumerate all candidate by DFS
#cat ${raw_dir}/trn.full | python gen_cand.py > ${raw_dir}/trn.cand
#cat ${raw_dir}/tst.full | python gen_cand.py > ${raw_dir}/tst.cand
#cat ${raw_dir}/dev.full | python gen_cand.py > ${raw_dir}/dev.cand
##add negative cases into training && testing
#python enlarge.py ${raw_dir}/trn.cand ${raw_dir}/trn.seg ${raw_dir}/trn.txt --train > ${raw_dir}/trn.la
#python enlarge.py ${raw_dir}/tst.cand ${raw_dir}/tst.seg ${raw_dir}/tst.txt --test > ${raw_dir}/tst.la
#python enlarge.py ${raw_dir}/dev.cand ${raw_dir}/dev.seg ${raw_dir}/dev.txt --test > ${raw_dir}/dev.la

#generate feature template
mkdir ${vec_dir}
dict_lst="${data_dir}/loc.dict ${res_dir}/province.dict ${res_dir}/city.dict ${res_dir}/county.dict ${res_dir}/town.dict ${res_dir}/district.dict ${data_dir}/sougou.dict ${data_dir}/brown.dict"
cat ${raw_dir}/trn.la | python gen_ftr_template.py ${temp} ${dict_lst} ${work_dir}/trn.struct > ${vec_dir}/trn.temp
cat ${raw_dir}/tst.la | python gen_ftr_template.py ${temp} ${dict_lst} ${work_dir}/tst.struct > ${vec_dir}/tst.temp
cat ${raw_dir}/dev.la | python gen_ftr_template.py ${temp} ${dict_lst} ${work_dir}/dev.struct > ${vec_dir}/dev.temp
#generate feature vector from the templates
cat ${vec_dir}/trn.temp | python gen_ftr_dict.py ${vec_dir}/ftr2id_thr${thr}.pkl ${vec_dir}/ftr2freq.csv ${vec_dir}/ftr2id_thr${thr}.csv ${thr} 
cat ${vec_dir}/trn.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/trn_thr${thr}.vec
cat ${vec_dir}/tst.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/tst_thr${thr}.vec
cat ${vec_dir}/dev.temp | python template2vec.py ${vec_dir}/ftr2id_thr${thr}.pkl > ${vec_dir}/dev_thr${thr}.vec
eps=0.5
##C=5
##for C in 0.005 0.05 0.5 5 50 500
##for C in 500 50 5 0.5 0.05 0.005 0.0005 0.00005 0.000005 0.0000005 
for C in 0.005
do
	conf=c${C}e${eps}_${misc}
	mkdir ${out_dir}/${conf}
	echo "" > ${out_dir}/${conf}/log_svm.txt
	echo "" > ${out_dir}/${conf}/log.csv
	${svm_dir}/svm_rank_learn -c ${C} -e ${eps} ${vec_dir}/trn_thr${thr}.vec ${mod_dir}/${conf}.model
	${svm_dir}/svm_rank_classify ${vec_dir}/dev_thr${thr}.vec ${mod_dir}/${conf}.model ${out_dir}/${conf}/dev.out >> ${out_dir}/${conf}/log_svm.txt 
	${svm_dir}/svm_rank_classify ${vec_dir}/tst_thr${thr}.vec ${mod_dir}/${conf}.model ${out_dir}/${conf}/tst.out >> ${out_dir}/${conf}/log_svm.txt 
	for topK in 5 20
	do
		echo "dev result" >> ${out_dir}/${conf}/log.csv  
		python recover_rank.py ${out_dir}/${conf}/dev.out  ${vec_dir}/dev.temp ${out_dir}/${conf}/dev.cand${topK} ${topK} >> ${out_dir}/${conf}/log.csv 
		echo "test result" >> ${out_dir}/${conf}/log.csv  
		python recover_rank.py ${out_dir}/${conf}/tst.out  ${vec_dir}/tst.temp ${out_dir}/${conf}/tst.cand${topK} ${topK} >> ${out_dir}/${conf}/log.csv 
	done
done
