#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_data.py
# Creation Date : 18-07-2014
# Last Modified : Wed Jul 23 11:54:19 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, io
import codecs
from itertools import *

word2abbr = {}
def valid_abbr(cand, abbr):
	if len(cand) < len(abbr) or not len(abbr):
		return 0 
	tag_lst = [0]*len(cand)
	i, j = 0, 0
	for i in range(0, len(cand)):
		if abbr[j] == cand[i]:
			j += 1
			tag_lst[i] = 1
		if j == len(abbr):
			break
	if j == len(abbr):
		return 1
	return 0

def canon(input_f):
	p = re.compile(u"(\S+):(\S+)\n")
	for l in input_f:
		m = p.match(l)
		if m:
			cand = m.group(1)
			abbr_lst = m.group(2).split(",")
			val_set = set() 
			for abbr in abbr_lst:
				if valid_abbr(cand, abbr):
					val_set.add(abbr)
			if len(val_set):
				if cand in word2abbr:
					word2abbr[cand] |= val_set 
				else:
					word2abbr[cand] = val_set
	for cand in word2abbr:
		val_set = word2abbr[cand]
		print cand + ":" + ",".join([abbr for abbr in val_set])

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	canon(input_stream)

if __name__ == "__main__": main()
