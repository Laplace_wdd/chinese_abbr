#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_data.py
# Creation Date : 18-07-2014
# Last Modified : Wed Jul 23 18:03:22 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, io
import codecs
from itertools import *

qid = 1
p = re.compile(u"[ -~]+")

def valid(cand):
	res = []
	for c in cand:
		if p.match(c) and len(res) and p.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def gen_align(cand, abbr, fid):
	global qid
	cand, abbr = valid(cand), valid(abbr)
	tag_lst = [1]*len(cand)
	i, j = 0, 0
	for i in range(0, len(cand)):
		if abbr[j] == cand[i]:
			j += 1
			tag_lst[i] = 2
		if j == len(abbr):
			break
	if j != len(abbr):
		print >> sys.stderr, "Wrong!"
	for i in range(0,len(tag_lst)):
		print str(tag_lst[i]) + u" qid:" + str(qid) + u" fid:" + str(fid) + u" " + cand[i]
	qid += 1

def canon(input_f):
	p, fid = re.compile(u"(\S+):(\S+)\n"), 1
	for l in input_f:
		if l == u"\n": continue 
		m = p.match(l)
		cand = m.group(1)
		abbr_lst = m.group(2).split(",")
		for abbr in abbr_lst:
			 gen_align(cand, abbr, fid)
		fid += 1

def main():
	input_stream = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	canon(input_stream)

if __name__ == "__main__": main()
