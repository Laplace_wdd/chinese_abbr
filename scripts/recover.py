#! /usr/bin/python
# --------------------------------------- 
# File Name : recover.py
# Creation Date : 20-07-2014
# Last Modified : Wed Jul 23 22:35:22 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, codecs
from itertools import *

def proc(lb_f, seq_f):
	p = re.compile(u"\d qid:(\d+) fid:\d+ (\S+)") 
	sent, abbr, sid = u"", u"", 0
	for lb, seq in izip(lb_f, seq_f):
		m = p.match(seq)
		c_sid = m.group(1)
		cha = m.group(2)
		if c_sid != sid:
			if sid != 0:
				yield sent + ":" + abbr
				sent, abbr = u"", u""
			sid = c_sid
		sent += cha
		if lb.strip() == u"2":
			abbr += cha
	yield sent + ":" + abbr

def main():
	lb_f = codecs.open(sys.argv[1], 'r', encoding='utf-8') 
	seq_f = codecs.open(sys.argv[2], 'r', encoding='utf-8') 
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	for l in proc(lb_f, seq_f):
		print l

if __name__ == "__main__": main()
