#/bin/bash
# --------------------------------------- 
# File Name : run.sh
# Creation Date : 19-07-2014
# Last Modified : Sat Aug 16 09:14:50 2014
# Created By : wdd 
# --------------------------------------- 

data_dir=../data
res_dir=${data_dir}/resource
raw_dir=${data_dir}/raw
work_dir=${data_dir}/plhmm
data_set=all_unlabeled
clus=2
##tag the training data using the current dictionary and transition probability
#cat ${data_dir}/unlabeled/${data_set}.seg | python parse.py ${data_dir}/sougou.dict ${res_dir}/type_dict.txt --train > ${data_set}.input
cat ${work_dir}/${data_set}.input | python baum_welch.py ${work_dir}/trans.dat ${work_dir}/emi.dat 4 10000
###cat ${work_dir}/${data_set}.input | python viterbi_tagger.py ${work_dir}/trans.dat.10 ${work_dir}/emi.dat.10 > tst.out
###tag the target data set using the current model
##cat ${raw_dir}/trn.la | python parse.py ${data_dir}/sougou.dict ${res_dir}/type_dict.txt --test > ${work_dir}/dev.input #| python viterbi_tagger.py ${work_dir}/trans.dat.10 ${work_dir}/emi.dat.10 > ${work_dir}/trn.struct 
##cat ${raw_dir}/tst.la | python parse.py ${data_dir}/sougou.dict ${res_dir}/type_dict.txt --test > ${work_dir}/tst.input #| python viterbi_tagger.py ${work_dir}/trans.dat.10 ${work_dir}/emi.dat.10 > ${work_dir}/tst.struct 
##cat ${raw_dir}/dev.la | python parse.py ${data_dir}/sougou.dict ${res_dir}/type_dict.txt --test > ${work_dir}/dev.input #| python viterbi_tagger.py ${work_dir}/trans.dat.10 ${work_dir}/emi.dat.10 > ${work_dir}/dev.struct 
#cat ${raw_dir}/trn.la | python parse.py ${data_dir}/sougou.dict ${res_dir}/type_dict.txt --test | python viterbi_tagger.py ${work_dir}/trans.dat ${work_dir}/emi.dat > ${work_dir}/trn.struct 
#cat ${raw_dir}/tst.la | python parse.py ${data_dir}/sougou.dict ${res_dir}/type_dict.txt --test | python viterbi_tagger.py ${work_dir}/trans.dat ${work_dir}/emi.dat > ${work_dir}/tst.struct 
#cat ${raw_dir}/dev.la | python parse.py ${data_dir}/sougou.dict ${res_dir}/type_dict.txt --test | python viterbi_tagger.py ${work_dir}/trans.dat ${work_dir}/emi.dat > ${work_dir}/dev.struct 
