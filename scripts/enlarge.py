#! /usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------- 
# File Name : gen_data.py
# Creation Date : 18-07-2014
# Last Modified : Wed Aug  6 10:19:53 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, io, codecs, random
from itertools import *

vp, tt, val = re.compile(u"[ -~]+"), 0.0, 0.0

def valid(cand):
	res = []
	for c in cand:
		if vp.match(c) and len(res) and vp.match(res[len(res)-1]):
			res[len(res)-1] += c
		else:
			res += [c]
	return res

def proc(cand_f, seg_f, pos_f):
	global tt, val
	lp = re.compile(u"(\S+):(\S+)\n")
	for cand, seg, pos in izip(cand_f, seg_f, pos_f):
		tt += 1
		m = lp.match(pos)
		root_lst = valid(m.group(1))
		if len(root_lst) > 10: continue 
		root = u" ".join(root_lst)
		val += 1
		cand_lst = cand.rstrip(u",\n").split(u",")
		seg = seg.strip()
		pos_str = m.group(2)
		pos_set = set(pos_str.split(u","))
		pos_lst, neg_lst, res_lst = [], [], []
		for abbr in cand_lst:
			abbr_ori = u"".join(abbr.split())
			if abbr_ori in pos_set:
				pos_lst += [abbr]
			else:
				neg_lst += [abbr]
		for abbr in pos_lst:
			res_lst += ["2::" + abbr]
		if sys.argv[4] == "--train":
			random.shuffle(neg_lst)
			if not len(pos_lst):
				res_lst += ["2::" + root]
				pos_lst += ["2::" + root]
			neg_lst = neg_lst[0:10*len(pos_lst)]
		for abbr in neg_lst:
			res_lst += ["1::" + abbr]
		print seg + u"##" + u",".join(res_lst)
	print >> sys.stderr, str(tt) + " out of " + str(val) + " remained " + str(val/tt)

def main():
	cand_f = codecs.open(sys.argv[1], 'r', encoding='utf-8') 
	seg_f = codecs.open(sys.argv[2], 'r', encoding='utf-8') 
	pos_f = codecs.open(sys.argv[3], 'r', encoding='utf-8') 
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sys.stderr = codecs.getwriter("utf-8")(sys.stderr)
	proc(cand_f, seg_f, pos_f)
	cand_f.close()
	seg_f.close()
	pos_f.close()

if __name__ == "__main__": main()
