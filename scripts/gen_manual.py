# /usr/bin/python
# --------------------------------------- 
# File Name : preprocess.py
# Creation Date : 06-02-2014
# Last Modified : Wed Jul 30 14:27:05 2014
# Created By : wdd 
# --------------------------------------- 
import sys, re, string, pickle, random, codecs
from itertools import *
batch=1000
#def main():
#	sys.stdin = codecs.getreader('utf-8')(sys.stdin)
#	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
#	sents = []
#	lp = re.compile(u"(\S+):(\S+)\n")
#	for sent in sys.stdin:
#		m = lp.match(sent)
#		query = m.group(1)
#		abbr_lst = m.group(2).split(u",")
#		sents += [[query, abbr_lst]]
#	random.shuffle(sents)
#	len_sents, portion = len(sents), len(sents)/batch + 1
#	print >> sys.stderr, len_sents
#	for i in range(0, portion):
#		print "\n".join([u"{0},{1},{2},{2}".format(str(i), query, " ".join(abbr_lst)) for query, abbr_lst in sents[batch*i:batch*i+batch]])
def main():
	sys.stdin = codecs.getreader('utf-8')(sys.stdin)
	sys.stdout = codecs.getwriter("utf-8")(sys.stdout)
	sents = []
	lp, o_flt, cnt = re.compile(u"(\d+),(.*),,(.*)\n"), u"", 0
	for sent in sys.stdin:
		m = lp.match(sent)
		flt = m.group(1)
		query = m.group(2)
		abbr_lst = m.group(3)
		if o_flt == u"": o_flt = flt
		if flt != o_flt:
			cnt = 0
			o_flt = flt
		if cnt >= batch:
			print u"{0},{1},{2},{2}".format(flt, query, abbr_lst)
		cnt += 1

if __name__ == "__main__": main()
